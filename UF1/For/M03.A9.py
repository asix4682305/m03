# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos
# Data: 2024-02-07 
#
# Versió: 1
#
# Descripció: Programa que llegeix dos nombres enters positius (n i m) i calcula els
# n primers múltiples d'm. El programa genera els múltiples de m sumant cada vegada
# m al múltiple anterior.
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      2   6               6, 12
#
#       Execució 2      6   2               2, 4, 6, 8, 10, 12

n=int(input('Introduce un numero entero positivo: '))
m=int(input('Introduce otro numero entero positivo: '))

multiples=[]

contador=0

while contador<n:
    valor_multiple=m+contador*m
    multiples.append(valor_multiple)
    contador=contador+1

print (multiples)

# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos
# Data: 2024-02-07 
#
# Versió: 1
#
# Descripció: Troba els nombres primers menors que 
# n i altre troba els n nombres primers.
#
# Especificacions d'entrada: Nombre sencer
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      A) 20               3, 5, 7, 11, 13, 17, 19
#
#       Execució 2      B) 3                3, 5, 7

programa=input('Programa A (menors de n) o programa B (n nombres)[A/B]: ')
if programa=='A':
    
    n=int(input('Introduce un número: '))
    primers=[]
    
    for numero in [3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]:
        if numero<n:
            primers.append(numero)
        else:
            print(primers)
            break #Com hem de utilitzar un break, es millor fer-ho amb while.
    
if programa=='B':

    n=int(input('Introduce un número: '))
    n100_primers=[3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    primers=[]

    for i in range(m):
        primers.append(n100_primers[i])
    
    print(primers)

#3. Indica què passarà en els següents fragments de programa:

#3.1. Exemple 1:
# En aquest exemple, el bucle exterior s'iterarà del 0 al 4 (inclusive),
# pero el bucle interior no és un rang, per tant no s'executarà i com el
# print es troba al seu interior, no farà print. 
print('exemple 1')
for i in range(0, 5):
    for j in range(0):
        print(i, j)

#3.2. Exemple 2:
# En aquest exemple, la variable i agafarà valors dins del rang 0-3 i amb aquest valor
# (màxim 2), anirà al bucle interior que escriurà (print), sent i el n que ha agafat
# i dins del rang 0-2 i sent j tots els n que van de la i actual a 3. Després,
# el programa tornarà al bucle exterior i agafarà el següent valor de i, i tornarà a
# fer el mateix.
print('exemple 2')
for i in range (0, 3):
    for j in range (i, 3):
        print (i, j)

#3.3. Exemple 3:
# En aquest exemple, i pren un valor del 0-4. Després, el programa avança fins al
# bucle interior on es faran iteracions en els nombres que siguin més grans que i
# i més petits que 3 i es faran prints d'aquests. Després d'això, el programa
# tornarà a agafar valors per a i i farà el mateix. Però aquest programa acabarà quan
# i=3, ja que serà més gran que el màxim del rang del bucle interior, llavors no farà
# ni iteracions ni print. El rang interior va de i a 2.
print('exemple 3')
for i in range(0, 5):
    for j in range(i, 3):
        print(i, j)

# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-07 
#
# Versió: 
#
# Descripció: 
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1
#
#       Execució 2

n = int(input('Introduce un numero: '))
contador = 0

while n != 0:
    contador=contador+1
    n=n//10

print("La cantidad de dígitos:", contador)
