chuletam03.txt
Noah Martos Teruel ASIX M03

1. Intèrpret interactiu
    Serveix per a que el shell executi els programes python.

    Per a iniciar l'interpret, introduim 'python3' al shell de linux.

    Dins de l'intèrpret es poden realitzar operacions matemàtiques bàsiques
    amb operadors binaris (+, -, *, /)

    Tractem dos tipus de dades: integer (sencer) i float (decimal)

    També tenim operadors unaris: + i -. Aquests canvien el signe dels nombres.

    Tenim operadors especials:  // divisió sencera (redondea siempre a lo bajo)
                                i % residu d'una divisió (R*divisor).

2. Intèrpret interactiu: variables
    Es poden asignar variables:
        num1 = 2
    Després es poden utilitzar:
        num2 = num1 + 2

3. Especificacions d'entrada, joc de proves i seguiment
    1. Llegibilitat-> utilitzar variables que descriguin el seu contingut, hacer comments (#)
    2. Capçelera-> Informació útil: shebang (#! /usr/bin/python3) necessari per a l'execució.
                    Informació del programador i la data, versió i descripció del programa.
    3. Especificacions d'entrada-> Especifica com i amb quin tipus de dades funciona el codi.
    4. Joc de proves-> Serveix per a verificar el funcionament del programa.
        Especifiquem quins resultats haurien de sortir amb determinats inputs per a comparar.
    5. Seguiment-> taula on es mostra la evolució dels valors de les diferents variables.

4. Expressions booleanes
    Existeixen dos valors booleans= True i False. Aquests es poden utilitzar en el codi.
    not True = False
    not False = True
    
    and (+), or (-)
    
    True and True = True
    True and False = False
    False and False = False
    
    True or True = True
    True or False = True
    False or False = False

5. Estructures alternatives (condicionals)
    if: equivale a 'si', poner una condición despues (ej. if n>8)
        if n>=18
        print('Major d'edat')

    elif: equivale a 'sino, si', tiene que haber un if antes y se leerá solo cuando la
            condición anterior no se haya cumplido y funciona igual que la anterior.
        elif n<=5:
            print('Bebé')
    else: equivale a 'sino' si ninguna de las condiciones anteriores se ha cumplido,
            entonces se ejecuta.
        else:
            print('Major d'edat')

    -programes amb strings:
    nombre1>nombre2 -> ordenar por orden alfabetico

    v_ascii=ord(letra)-> transforma un caracter en su código ascii
        if 65<=v_ascii<=90:
            print ('Majúscula')
        else:
            print('Minúscula')
        
    if contraseña1==contraseña2:
        print('Correcte')
    else:
        print('Les contrasenyes no coincideixen.')

6. Estructures repetitives: while
    Mentre una condició s'acomleixi, el bucle es seguirà iterant.
    Per a que el programa no peti, hem d'assegurar-nos de que el bucle es pararà.

    SOLO se ejecuta si la condición se cumple, sino, nunca se ejecutará.

    -Utilitzant flags:
        Bucle=True
            while Bucle==True:
                (programa)
                if (condició)
                Bucle=False

    -Utilitzant comptadors:
        contador=0
            while contador<10:
                (programa)
                contador=contador+1

    -Utilitzant 'break':
        while True:
            (programa)
            if(condició):
                break

7. Estructures repetitives: do ... while
    Esta estructura no existe en python, pero es muy útil, ya que el bucle SE EJECUTA 
    AL MENOS UNA VEZ.
    Hay una forma de emularlo en python:

    Combinando el while True y el break:

    while True:
        (codigo)
        if (condicio):
            break

8. Estructures repetitives: for ... in 
    Esta estructura sirve para iterar sobre una secuencia (lista, tupla, 
    diccionario, set o string). 

    -Lista:
        fruits=["apple", "banana", "cherry"]
        for x in fruits:
            print(x)

        >>apple
        >>banana
        >>cherry

    -String:
        for x in banana
            print(x)
        
        >>b
        >>a 
        >>n 
        >>a 
        >>n 
        >>a 
    
    -Break: también se puede usar 'break' en los bucles for, pero cuando esto sea necesario,
    seguramente lo mejor será utilizar un while. 

    fruits=["apple", "banana", "cherry"]
    for x in fruits:
        if x == "banana":
            break
        print(x)

    >>apple

    -Continue: sirve para 'saltarse' una iteración del bucle. 
    fruits=["apple", "banana", "cherry"]
    for x in fruits:
        if x == "banana":
            continue
        print(x)
    
    >>apple
    >>cherry

    -Range: Devuelve una secuencia de numeros, default empezando por 0 y haciendo
    tantas iteraciones como hayamos especificado en el rango es decir, si ponemos
    range(6), obtendremos los valores de 0 al 5.

    for x in range(4):
        print(x)
    
    >>0
    >>1
    >>2
    >>3

    -También podemos elegir otro incio:

    for x in range(2,4):
        print(x)

    >>2
    >>3

    -Y también escoger el incremento:

    for x in range(2, 30, 3):
        print(x)

    >>2
    >>5
    >>8
    >>11
    >>14
    >>17
    >>20
    >>23
    >>26
    >>29
    
    -else: podemos utilizar el else para hacer algo cuando el bucle for acabe.

    for x in range(6):
        print(x)
    else:
        print("se acabó")

    -Loop anidados: El loop interior se ejecutará una vez por cada iteracio del loop exterior.
    
    adj=["red", "big", "tasty"]
    fruits=["apple", "banana", "cherry"]

    for x in adj:
        for y in fruits:
            print(x, y)

    >>red apple
    >>red banana
    >>red cherry
    >>big apple
    >>big banana
    >>big cherry
    >>tasty apple
    >>tasty banana
    >>tasty cherry

9. Cadenes

abs: Valor absolut d'una variable.
    nombre=abs(nombre)

Última xifra: utilitzem % per a agafar l'última xifra
    while nombre>10:
        suma= nombre%10
        nombre=nombre//10

Saber lenght: Dividint
    while nombre>0:
        nombre=nombre//10
        comptador=comptador+1

Treure signe: utilitzem []
    if nombre[0]=='+' or nombre[0]=='-':
        nombre=nombre[1:]

Sumatori de xifres:
    for caracter in nombre:
        suma=suma+int(caracter)

Ús de cometes:
    Cometes dobles: tal cual 
    Cometes simples: literal, es pot utilitzar \ per a que el següent caràcter
        perdi les seves propietats especials.
            print('l\'altre')
            print('això\tés un tabulador')

Operadors sobre cadenes:
    concatencació=cadena+cadena

    repetició=nombre*cadena

    python3

    >>>3+4
    7
    >>>str(3+4)
    '7'
    >>>str(3)+str(4)
    '34'
    >>>'3'+'4'
    '34'
    >>>int('3') + float('4')
    7.0
    >>>3+str(4)
    Error
    >>>int('3.2')
    Error
    >>>int(3.2)
    3

ord: converteix caracter en el seu codi ascii
    ord(c)

chr: converteix nombre ascii en el seu caracter
    chr(n)

len: dona la longitud de la cadena
    len('hola')

Indexació: numeració de caràcters
    m=hola
    m[0]='h'
    m[1]='o'
    m[2]='l'
    m[3]='a'

    m[-1]='a'
    ...

Recorrer una cadena:
    Caracter a caracter:
        for c in cadena:
            print(c)
    
    Recorrer per posició:
        for pos in range(0, len(cadena)):
            print(pos, cadena[pos])

10. Comparació i slicing de cadenes. Control d'errors.

Comparació de cadenes: Es compara el codi ascii de les cadenes.
    python3
    >>>'b'>'a'
    True 

    >>>'A'>'a' 
    False

    >>>'b'>'abcde'
    True (compara el primer caracter)

    >>>'bca'>'bcd'
    False, compara els primers, els segons, així fins q hi ha una diferencia.

Slicing:
    Operador[n:]-> eliminar caràcters.

    a[inici:final] -> retalla des del caràcter inici fins el caràcter final
    a[inici:] -> retalla des d'inici fins l'últim caràcter
    a[:final] ->retalla des del primer caràcter fins a final
    a[:]-> fa una copia de tota la cadena

    python3
    >>>help(str)
    Manual per als mètodes que es poden utilitzar amb els str a python

    a='hola'

    >>>a.isdigit()
    False
    >>>a.upper()
    'HOLA'
    >>>a.islower()
    True
    >>>a.find('ol')
    1
    >>>a.count('3')
    0
    >>>a.index('o')
    1
    >>>a.find('1')
    -1
    >>>a.index(1)
    Error

f-strings: Formatea cadenes, substituir els noms de les variables pel seu valor.

nom='Noah'

f'Em dic {nom}'
f'Resultat={3+5}'

h=9
m=10

f'hora:{h:5}, minut{m:5}' ->omple el que falta per 5 espais en blanc
f'hora:{h:02}, minut{m:02}' -> omple el que falta per 5 espais en blanc

Control d'errors:
    Què passa si no respectem les especificacions d'entrada?
    Fer una cosa que no té sentit o llençar un error d'execució.

    Evitar que es produeixi un error:
    if dni.isdigit() ==False:
        print(ERROR)

    elif dni c[0]=='0':
        print(ERROR)

    else:
        (programa)

    Exit: acabar un programa quan les dades no son adeqüades. Son codis d'error.

    if dni.isdigit() ==False:
        print(ERROR)
        exit(1)

    elif dni c[0]=='0':
        print(ERROR)
        exit(2)

    Joc de proves del control d'error

    Entrada         T/F 
    12345678        T 
    123456789       F 
    -12345678       F 
    no tinc dni     F 

11. Tupla 
Com una llista, però les seves dades son immutables. No pots modificar el valor
dels seus elements.

A diferència de les llistes que es creen amb [], les tuples es crean amb ().

data=(4, 2, 2021)
