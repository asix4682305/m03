# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 1
#
# Descripció: Escriure tots els divisors d'un nombre en ordre ascendent.
#
# Especificacions d'entrada: Un nombre sencer
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      12                  1 2 3 4 6 12
#
#       Execució 2      -12                 -12 -6 -4 -3 -2 -1

n=int(input('Introduce un número entero: ')) #User input

contador=1 #No dividimos entre 0, porque eso daria math error.

divisores=[] #Creamos una lista para ir añadiendo los valores que sean divisores.

if n==0: #Si el número es 0, no se puede dividir.
    print('0 no se puede dividir.')


elif n<0: #Si es negativo
    
    contador= -n
    #print(contador)

    while contador>0:
        if n%contador==0:
            #print(n%contador)
            divisores.append(-contador)
            #print(divisores)

        contador=contador-1
        #print(contador)
        

else: #Si es positivo

    while contador<=n: #Hasta que hayamos llegado al número introducido
        if n%contador==0: #Divimos entre el contador, si da exacto
            divisores.append(contador) #Añadimos el divisor a la lista.
        
        contador=contador+1 #Sumamos 1 al contador



print(divisores)