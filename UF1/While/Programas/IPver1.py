# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-30 
#
# Versió: 1
#
# Descripció: Programa que llegeix 4 nombres enters i determina si son els 4
# camps que conformen una ip vàlida. Una expressió booleana diu di els 4
# camps corresponen a una adreça vàlida o no.
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      192 168 0 1         True
#
#       Execució 2      2 -3 34 729         False

contador=1

while contador<=4:
    octet=int(input(f'Introduce el octeto número {contador}: '))
    if 255<octet or 0>octet:
        print('False')
        break
    else:
        contador=contador+1
        continue

#print(contador)

if contador==5:
    print('True')