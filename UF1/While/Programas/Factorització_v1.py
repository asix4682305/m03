# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-01 
#
# Versió: 1
#
# Descripció: Programa que retorna els factors primers d'un nombre (fa la factorització)
#
# Especificacions d'entrada: Un nombre sencer
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      12                  2, 2, 3


n=int(input('Introduce el número: '))

n_primos=[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
#Creamos una lista con los n primos con los que trabajaremos
factores=[] #Creamos una lista vacía donde meteremos los factores

factorizado=False #Flag para romper el bucle

while factorizado==False: 
    for n_actual in n_primos: #Bucle para recorrer una lista
        if n%n_actual==0: #Si la división no da residuo
            factores.append(n_actual) #Añadimos el numero a la lista vacía
            n=n/n_actual #Dividimos el numero
    if n==1: #Cuando el numero sea igual a 1
        factorizado=True #Rompemos el bucle

factores.sort() #Ordenamos los factores
print(factores) #Print