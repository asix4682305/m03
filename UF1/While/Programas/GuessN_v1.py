# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 1
#
# Descripció: El programa pensa un nombre aleatori del 0 al 100 i ajuda
# a l'usuari a endevinar-lo donant-li pistes.
#
# Especificacions d'entrada: Nombres sencers i positius.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      n=70
#                       50                  Higher
#                       90                  Lower
#                       70                  True

import random #Importamos la libreria random

n=random.randint(0,100) #Escogemos un valor int aleatorio entre 0 y 100.
#print(n)

adivinado=False #Creamos una variable que cuando sea True romperá el bucle.

while adivinado==False:
    userinput= int(input('Adivina el número: '))
    if userinput>n:
        print('Más bajo')
    
    elif userinput<n:
        print('Más alto')

    elif userinput==n:
        print('Acierto!')
        adivinado=True

        