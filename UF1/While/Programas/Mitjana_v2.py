# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 1
#
# Descripció: Programa que calcula la mitjana dels nombres positius.
# L'usuari introdueix la quantitat de notes que introduirà i després
# introdueix les notes.
#
# Especificacions d'entrada: Nombres.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      5
#                       5 3 4 -5 8          5
#
#       Execució 2      3
#                       5 5 5               5

N=int(input('Cuántas notas vas a añadir al cálculo de la media? '))

numeros_media= [] #Creamos lista vacía para ir añadiendo valores
contador=0 #Creamos contador

while contador<N:
    inputuser=float(input('Introduce un número: ')) 
    numeros_media.append(inputuser)
    contador=contador+1

media= sum(numeros_media)/N
media= round(media,2)
print(media)