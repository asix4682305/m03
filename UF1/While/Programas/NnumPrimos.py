# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-01 
#
# Versió: 1
#
# Descripció: Llegeix N nombres diu quin és el menor i en quina posició ho ha llegit.
#
# Especificacions d'entrada: Un numero entero
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      5, 3, 2, 8          El número 2, leido en 3a posición

N=4
contador=0

numeros_lista=[]

while contador<4:
    numero=input(f'Introduce un número: ')
    numeros_lista.append(numero)
    contador=contador+1

numeros_lista.sort

valor_menor=min(numeros_lista)
valor_menor_arreglado=valor_menor+1

posicion_valor=numeros_lista.index(valor_menor)

print(f'El valor más pequeño es {valor_menor}, leído en posición {posicion_valor}')