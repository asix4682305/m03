# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-30 
#
# Versió: 1
#
# Descripció: Programa que llegeix 4 nombres enters i determina si son els 4
# camps que conformen una ip vàlida. Una expressió booleana diu di els 4
# camps corresponen a una adreça vàlida o no.
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      192 168 0 1         True
#
#       Execució 2      2 -3 34 729         False

contador=1

while contador<=4:
    octet=int(input(f'Introduce el octeto número {contador}: '))
    if contador==1:
        octet1=octet
        contador=contador+1

    elif contador==2:
        octet2=octet
        contador=contador+1

    elif contador==3:
        octet3=octet
        contador=contador+1

    elif contador==4:
        octet4=octet
        contador=contador+1

    else:
        break
    

#print(contador)

ip=[octet1, octet2, octet3, octet4]

print(ip)

contador2=0

for octet in ip:
    if octet < 0 or octet > 255:
        print('False')
        break

else:
    print('True')