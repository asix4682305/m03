# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-30 
#
# Versió: 2
#
# Descripció: Programa que llegeix 4 nombres enters i determina si son els 4
# camps que conformen una ip vàlida. Una expressió booleana diu di els 4
# camps corresponen a una adreça vàlida o no. Sense bucle.
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      192 168 0 1         True
#
#       Execució 2      2 -3 34 729         False

octet1=int(input('Introduce el primer octeto de la dirección IP: '))
octet2=int(input('Introduce el segundo octeto de la dirección IP: '))
octet3=int(input('Introduce el tercer octeto de la dirección IP: '))
octet4=int(input('Introduce el cuarto octeto de la dirección IP: '))

if octet1<0 or octet1>255:
    print('False')

elif octet2<0 or octet2>255:
    print('False')

elif octet3<0 or octet3>255:
    print('False')

elif octet4<0 or octet4>255:
    print('False')

else:
    print('True')