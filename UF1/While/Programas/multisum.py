# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 1
#
# Descripció: Progrma que multiplica dos nombres sencers en base a sumes succesives.
#
# Especificacions d'entrada: Dos nombres.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      4 -5                -20
#
#       Execució 2      -5 -4               20

factor1= int(input('Introduce un factor de multiplicación: ')) #User input
factor2= int(input('Introduce el otro factor de multiplicación: ')) #User input

Negativo=False #Flag para guardar si factor2 (que se usa para romper el bucle) es
                #negativo, poder cambiarlo.

if factor2<0:   #Si es negativo, lo ponemos positivo para asegurarnos de que
    Negativo=True # se hacen todas las sumas. Guardamos en 'negativo=false'
    factor2=-factor2 #para cambiar el signo al final.

contador=0  #Contador para romper el bucle.

valores=[]  #Lista para ir acumulando valores

while contador < factor2: #Hasta que tengamos (factor2) valores
    valores.append(factor1) #Añadimos factor1 a la lista
    #print(valores)
    contador=contador+1 #Sumamos uno al contador

if Negativo==True:  #Si factor2 era negativo
    producto= -sum(valores) #Multiplicamos el resultado de la suma por -1

elif Negativo==False: #Si factor2 era positivo
    producto= sum(valores) #Sumamos los valores de la lista

print(producto) #Resultado