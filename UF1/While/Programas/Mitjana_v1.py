# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 1
#
# Descripció: Programa que calcula la mitjana dels nombres positius.
# Llegeix nombres positius fins que s'introdueix un negatiu. En aquell
# moment es calcula la mitjana dels nombres que s'han entrat.
#
# Especificacions d'entrada: Nombres sencers i finalment un negatiu.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      5 3 5 -5            4
#
#       Execució 2      5 -8                5

numeros_media= [] #Creamos lista vacía para ir añadiendo valores

while True:
    inputuser=float(input('Introduce un número: ')) 
    if inputuser>0: #Cuando sea un valor válido
        numero= inputuser   #Añadimos el valor a una variable
        numeros_media.append(numero)    #Añadimos variable a la lista
    else:
        media=sum(numeros_media) / len(numeros_media)   #Calculamos la media
        media= round(media, 2)
        print(media)
