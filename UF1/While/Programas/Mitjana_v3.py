# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 3
#
# Descripció: Programa que calcula la mitjana dels nombres positius.
# El primer nombre que introdueix l'usuari indica la quantitat de nombres.
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      5 5 3 4 -5 8        5
#
#       Execució 2

num1= int(input('Introduce un número: '))
contador=0
numeros_media=[]

while contador<num1:
    inputuser=float(input('Introduce un número: ')) 
    numeros_media.append(inputuser)
    contador=contador+1

media= sum(numeros_media)/num1
media= round(media,2)
print(media)