# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-01-31 
#
# Versió: 1
#
# Descripció: Llegeix un nombre i diu si és primer.
#
# Especificacions d'entrada: Nombre positiu sencer.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      5                   Prim
#
#       Execució 2      8                   No prim

n=int(input('Introduce tu número: ')) #User input

primo=False #Flag
contador=n-1
#print (contador)

while primo==False:
    if contador==1:
        print('Primo')
        primo=True

    elif n%contador==0:
        print('No primo')
        break

    else:
        contador=contador-1
        #print(contador)

        