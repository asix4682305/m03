# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-01 
#
# Versió: 1
#
# Descripció: Programa que retonar els n primers abans de l'input
# Especificacions d'entrada: Un nombre sencer
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      10                  2, 3, 5, 7


n=int(input('Introduce el número: '))

n_primos=[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
#Creamos una lista con los n primos que usaremos
factores=[] #Creamos una lista vacía en la que añadir los factores resultantes

finish=False #Flag para romper el bucle

while finish==False: 
    for n_actual in n_primos: #Bucle para recorrer toda la lista
        if n_actual<n: #Si el n es menor a l'input
            factores.append(n_actual) #Añade a la lista vacía
        
    finish=True #Rompe el bucle (cuando se haya recorrido una vez la lista
                #por lo que no hace falta if)

#factores.sort()
print(factores) #Print