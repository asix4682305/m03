#! /usr/bin/python3
#Noah Martos Teruel

import csv
import sys
import os
import time
import json

def cargarCSVenDicts(csv_input_file=str):
    """Dado un fichero .csv, devuelve una lista con diccionarios en cada índice.

    Args:
        csv_input_file (str): fichero csv del que se quiere extraer la información.

    Returns:
        lista_dicts: lista con diccionarios provenientes del fichero .csv
        False: No se ha encontrado el fichero.
    """
    #Esta función utiliza el metodo DictReader para cargar información de un fichero csv a
    #una lista con un diccionario por cada línea del fichero csv.
    try:
        lista_dicts=[]
        with open(csv_input_file, newline='') as csv_fichero:
            reader= csv.DictReader(csv_fichero)
            for row in reader:
                lista_dicts.append(row)
            return lista_dicts
    
    except FileNotFoundError:
        return False
    
def llistarAllDadesCSV(lista_diccionarios=list):
    """Dada una lista de diccionarios, crea una tabla mostrando todos los valores.

    Args:
        lista_diccionarios (list): Lista con diccionarios con las mismas keys dentro. Defaults to list.
    """
    #Este bucle for guarda en la lista columnas todas las claves diferentes de los diccionarios, para obtener las columnas.
    columnas = []
    for diccionario in lista_diccionarios:
        for clave in diccionario.keys():
            if clave not in columnas:
                columnas.append(clave)

    #Aquí usamos las claves del diccionario para crear un nuevo diccionario que tiene asignado el valor 0 por cada clave.
    longitudes={}
    for clave in diccionario.keys():
        longitudes[clave]=len(clave)

    #En este bucle conseguimos el valor con más carácteres de cada clave (columna), para poder escribir bien la tabla.
    for diccionario in lista_diccionarios:
        for clave in diccionario.keys():
            if len(diccionario[clave])>longitudes[clave]:
                longitudes[clave]=len(diccionario[clave])
    
    
    #Ahora ya podemos crear los encabezados con la longitud deseada para que quepan todos los valores.
    #center() es un metodo de python para asignar una longitud al espacio y colocar el texto en el medio.
    encabezados=""
    for col in columnas:
        encabezados += col.center(longitudes[col]) + "|"
    print(encabezados)

    
    for diccionario in lista_diccionarios:
        fila=""
        fila="|".join(str(diccionario.get(col, "")).center(longitudes[col]) for col in columnas)
        print(fila)

def desarDadesFitxer(lista_diccionarios=list, outputfile=str):
    """Tiene una función parecida a cargarAllDadesCSV, ya que genera el mismo output, pero en vez de mandarlo a la pantalla,
    guarda toda la información en un fichero txt.

    Args:
        lista_diccionarios (list): Lista de diccionarios.
        outputfile (str): Nombre del fichero que se generará.
    """
    with open(outputfile, "w") as pokemon:
        #Este bucle for guarda en la lista columnas todas las claves diferentes de los diccionarios, para obtener las columnas.
        columnas = []
        for diccionario in lista_diccionarios:
            for clave in diccionario.keys():
                if clave not in columnas:
                    columnas.append(clave)

        #Aquí usamos las claves del diccionario para crear un nuevo diccionario que tiene asignado el valor 0 por cada clave.
        longitudes={}
        for clave in diccionario.keys():
            longitudes[clave]=len(clave)

        #En este bucle conseguimos el valor con más carácteres de cada clave (columna), para poder escribir bien la tabla.
        for diccionario in lista_diccionarios:
            for clave in diccionario.keys():
                if len(diccionario[clave])>longitudes[clave]:
                    longitudes[clave]=len(diccionario[clave])
        
        
        #Ahora ya podemos crear los encabezados con la longitud deseada para que quepan todos los valores.
        #center() es un metodo de python para asignar una longitud al espacio y colocar el texto en el medio.
        encabezados=""
        for col in columnas:
            encabezados += col.center(longitudes[col]) + "|"
        print(encabezados, file=pokemon)

        
        for diccionario in lista_diccionarios:
            fila=""
            fila="|".join(str(diccionario.get(col, "")).center(longitudes[col]) for col in columnas)
            print(fila, file=pokemon)

def filtrarBD(lista_diccionarios=list):
    """Esta función muestra todas las keys que tiene un diccionario, utiliza la función menuUniversal
    para mostrarlas y dejar al usuario escoger una.

    Args:
        lista_diccionarios (lista): Lista con diccionarios que tienen las mismas claves.

    Returns:
        filtro_aplicar: Opción escogida por el usuario entre todas las claves del diccionario.
    """

    print("¿Qué filtro quieres aplicar?")

    columnas = []
    for diccionario in lista_diccionarios:
        for clave in diccionario.keys():
            if clave not in columnas:
                columnas.append(clave)

    filtros={}
    for i in range(1, len(columnas)+1):
        filtros[i]=columnas[i-1]

    opcion=menuUniversal(filtros)
    filtro_aplicar=filtros[opcion]
    return filtro_aplicar

def busquedaPorFiltro(lista_diccionarios=list, filter=str):
    
    valor=input(f"Introduce el valor de {filter} por el que quieres buscar: ")

    #Este bucle for guarda en la lista columnas todas las claves diferentes de los diccionarios, para obtener las columnas.
    columnas = []
    for diccionario in lista_diccionarios:
        for clave in diccionario.keys():
            if clave not in columnas:
                columnas.append(clave)

        #Aquí usamos las claves del diccionario para crear un nuevo diccionario que tiene asignado el valor 0 por cada clave.
    longitudes={}
    for clave in diccionario.keys():
        longitudes[clave]=len(clave)

        #En este bucle conseguimos el valor con más carácteres de cada clave (columna), para poder escribir bien la tabla.
    for diccionario in lista_diccionarios:
        for clave in diccionario.keys():
            if len(diccionario[clave])>longitudes[clave]:
                longitudes[clave]=len(diccionario[clave])
        
        
        #Ahora ya podemos crear los encabezados con la longitud deseada para que quepan todos los valores.
        #center() es un metodo de python para asignar una longitud al espacio y colocar el texto en el medio.
    encabezados=""
    for col in columnas:
        encabezados += col.center(longitudes[col]) + "|"
    print(encabezados)

    match=False

    diccionarios_filtrados=[]

    for diccionario in lista_diccionarios:
        fila=""
        if diccionario[filter]==valor:
            diccionarios_filtrados.append(diccionario)
            fila="|".join(str(diccionario.get(col, "")).center(longitudes[col]) for col in columnas)
            print(fila)
            match=True

    if match==False:
        print("Sin coincidencias.")

    return diccionarios_filtrados

def CreacionDiccionarios(ruta_archivo=str, elemento=str):
    """Funcion que convierte elementos de ficheros json en diccionarios python.

    Args:
        ruta_archivo (string): localización del archivo json.
        elemento (string): elemento que convertir en diccionario.

    Returns:
        diccionario: diccionario de python.
    """
    with open(ruta_archivo, "r") as archivo:
        datos_json = json.load(archivo)
    
    diccionario=datos_json[elemento]
    return diccionario

def menuUniversal(menu=dict, sleep=0):
    """Función que muestra datos con apariencia de menú y deja al usuario
    elegir una opción dentro de las existentes usando números.

    Args:
        menu (diccionario): diccionario de python con las opciones a mostrar.
        sleep (num): tiempo que pasa entre que aparece cada entrada del diccionario.

    Returns:
        opcion: opcion que ha elegido el usuario.
    """
    opciones=len(menu)

    while True:
        try:
            for x, y in menu.items():   #Display menu
                print(x, y)
                time.sleep(sleep)

            opcion=int(input('Introduce un número: '))
            if opcion <= opciones and opcion > 0:
                return opcion                
        except ValueError:
            print('Introduce un número de los indicados en pantalla.')
            os.system('clear')

def limpiarTerminal(sleep=0):

    """Función que limpia la terminal.

    Args:
        sleep (num): Tiempo que tarda en limpiarse la terminal.
    """
    time.sleep(sleep)
    os.system('clear')


def enterParaContinuar():
    """Espera a que el usuario le de a Enter para romper el bucle y continuar con el flujo.
    """
    while True:
        input("Pulsa Enter para continuar: ")
        break

def displayDicts(dictionary=dict):
    """Printea las claves claves y los valores respectivos de un diccionario.

    Args:
        stats (diccionario): Diccionario donde se encuentran las estadísticas.
        diccionario (diccionario, optional): Si quieres que algunas palabras sean sustituidas por otros valores, introduce
            un diccionario con claves (valores a sustituir) y sus valores respectivos. Defaults to None.
    """
    for x,y in dictionary.items():
        print(f"{x}: {y}")

def dicttoMenu(dictionary=dict):
    
    menuCreado={}
    opciones=len(dictionary)
    contador=1
    for x in dictionary.keys():
        menuCreado[contador]=x
        contador+=1

    return menuCreado

def listToMenu(lista=list):

    menuCreado={}
    contador=1
    for item in lista:
        menuCreado[contador]=item
        contador+=1
    
    return menuCreado

def matchValorDictLista(lista_dicc, elemento, valor):

    counter=0
    for dict in lista_dicc:
        if dict[elemento]==valor:
            return counter
        counter+=1

def sacarValoresDiferentesDictLista(lista_dicc, elemento):

    valores_diferentes=[]
    for i in range(0, len(lista_dicc)):
        if lista_dicc[i][elemento] not in valores_diferentes:
            valores_diferentes.append(lista_dicc[i][elemento])

    return valores_diferentes

def cambioEstadisticas(diccionario, elemento, valor):

    diccionario[elemento]=str(valor)
    return diccionario[elemento]

def cambioEstadisticaTotal(stats=list, dicc=dict):

    valor_total=0

    valores_a_sumar=[]
    for stat in stats:
        valores_a_sumar.append(int(dicc[stat]))

    for valor in valores_a_sumar:
        valor_total=valor_total+(valor)
    return str(valor_total)

def generarValoresEstadistica(list_dic=list, stat=str):

    valores_posibles=[]
    for elem in list_dic:
        if elem[stat] not in valores_posibles:
            valores_posibles.append(elem[stat])

    valores_totales=[]

    for elem in list_dic:
        valores_totales.append(elem[stat])

    estadistica={}
    
    for elem in valores_posibles:
        estadistica[elem]=valores_totales.count(elem)
    
    return estadistica

def calcularEstadisticaNum(estadistica_valores=dict, output_file=str):

    total=0
    for x, y in estadistica_valores.items():
        total=total+int(y)

    with open(output_file, "a") as estadisticas:
        for x, y in estadistica_valores.items():
            porcentaje=round((y/total)*100, 2)
            estadisticas.write(f"El {porcentaje}% de los Pokémon son {x}.\n")

def generarEstadisticaCombate(lista_dic=list, stat=int):
    
    valors_totales=[]

    for elem in lista_dic:
        valors_totales.append(int(elem[stat]))

    return valors_totales

def calcularEstadisticaCombate(valors_totales=list, outputfile=str):

    with open(outputfile, "w") as battlestats:
        valors_totales=sorted(valors_totales)

        total=len(valors_totales)

        maximo=max(valors_totales)

        minimo=min(valors_totales)

        media=sum(valors_totales)/total

        print(f"El valor máximo es {maximo}\nEl valor mínimo es {minimo}\nEl valor medio es {round(media, 2)}", file=battlestats)

##pruebas##
"""fichero_csv='./Pokemon.csv'
menus='./menu.json'
lista_dicts=cargarCSVenDicts(fichero_csv)

menucreado=dicttoMenu(lista_dicts[1])
print(menucreado)"""