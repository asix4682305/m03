#! /usr/bin/python3
#Author: Noah Martos Teruel

import tools
import sys

def main():
    tools.limpiarTerminal()
    fichero_csv='./Pokemon.csv'
    menus='./menu.json'
    lista_dicts=tools.cargarCSVenDicts(fichero_csv)

    if lista_dicts==False:
        print("No se ha encontrado el fichero")
        sys.exit(1)
    
    menuPrincipal=tools.CreacionDiccionarios(menus, 'menuPrincipal')

    """for valor in range(1, len(lista_dicts)):
        if str(valor)==lista_dicts[valor-1]['#']:
            pass
        else:
            print (f"Err:    {valor}, {lista_dicts[valor-1]}, {lista_dicts[valor-1]['#']}")
    
    tools.enterParaContinuar()"""
    #menu principal
    while True:
        tools.limpiarTerminal()
        print("Bienvenido a la base de datos de Pokémon.")
        opcion=tools.menuUniversal(menuPrincipal)
        match opcion:

            case 1:
                #listar
                tools.limpiarTerminal()
                menuListar=tools.CreacionDiccionarios(menus, 'menuLlistar')

                while True:
                    tools.limpiarTerminal()
                    opcion2=tools.menuUniversal(menuListar)
                    match opcion2:
                    
                        case 1:
                            #listarTodo
                            tools.limpiarTerminal()
                            tools.llistarAllDadesCSV(lista_dicts)
                            tools.enterParaContinuar()

                        case 2:
                            #filtrar
                            tools.limpiarTerminal()
                            filtro=tools.filtrarBD(lista_dicts)
                            tools.busquedaPorFiltro(lista_dicts, filtro)
                            tools.enterParaContinuar()
                                    
                        case 3:
                            break
            case 2:
                #modificar
            
                menuBusquedaModificacion=tools.CreacionDiccionarios(menus, 'menuBusquedaModificacion')
                opcion=tools.menuUniversal(menuBusquedaModificacion)

                if opcion== 3:
                    break

                elif opcion== 1:
                    while True:
                        try:
                            tools.limpiarTerminal()
                            numero_pkdx=int(input("Numero del Pokémon a modificar: "))
                            if numero_pkdx>0 and numero_pkdx<len(lista_dicts):
                                break
                            else:
                                print("Introduce un número válido.")
                        except ValueError:
                            print("Introduce un valor válido.")
                    tools.limpiarTerminal()
                    pokemon_a_modificar=lista_dicts[numero_pkdx-1]
                    indice_lista=numero_pkdx-1

                elif opcion== 2:
                    match=False
                    while True:
                        try:
                            tools.limpiarTerminal()
                            nombre_pokemon=str(input("Nombre del Pokémon a modificar: "))

                            for i in range (0, len(lista_dicts)):
                                
                                if lista_dicts[i]['Name']==nombre_pokemon:
                                    pokemon_a_modificar=lista_dicts[i]
                                    indice_lista=i
                                    match=True
                                    break
                            
                            if match==True:
                                break
                            
                        except ValueError:
                            print('Introduce un valor válido.')
                
                tools.displayDicts(pokemon_a_modificar)
                tools.enterParaContinuar()
                tools.limpiarTerminal()

                print("Qué valor quieres modificar?")
                menuModificacion=tools.dicttoMenu(pokemon_a_modificar)
                opcion=tools.menuUniversal(menuModificacion)
                match opcion:
                    case 1:
                        #numero pokedex
                        print("No se puede modificar el número de la Pokédex.")
                        tools.enterParaContinuar()

                    case 2:
                        while True:
                            #nombre pokemon
                            name=str(input("Introduce el nuevo nombre para este Pokémon: "))
                            if len(name)<25:
                                lista_dicts[indice_lista]['Name']=name
                                print("Cambio efectuado.")
                                tools.enterParaContinuar()
                                break
                            else:
                                print("El nombre no debe superar los 25 carácteres.")
                    
                    case 3:
                        #tipo 1
                        tipos_pokemon=tools.sacarValoresDiferentesDictLista(lista_dicts, 'Type 1')
                        menuTipos=tools.listToMenu(tipos_pokemon)

                        while True:
                            
                            opcion=tools.menuUniversal(menuTipos)

                            if menuTipos[opcion]==pokemon_a_modificar['Type 2']:
                                print("Un Pokémon no puede ser dos veces del mismo tipo.")
                                tools.enterParaContinuar()
                                tools.limpiarTerminal()

                            else:
                                lista_dicts[indice_lista]['Type 1']=menuTipos[opcion]
                                print('Cambio efectuado.')
                                tools.enterParaContinuar()
                                break
                    
                    case 4:
                        #tipo 2
                        tipos_pokemon=tools.sacarValoresDiferentesDictLista(lista_dicts, 'Type 1')
                        menuTipos=tools.listToMenu(tipos_pokemon)

                        while True:
                    
                            opcion=tools.menuUniversal(menuTipos)

                            if menuTipos[opcion]==pokemon_a_modificar['Type 1']:
                                print("Un Pokémon no puede ser dos veces del mismo tipo.")
                                tools.enterParaContinuar()

                            else:
                                lista_dicts[indice_lista]['Type 2']=menuTipos[opcion]
                                print('Cambio efectuado.')
                                tools.enterParaContinuar()
                                break
                    
                    case 5:
                        #total
                        print("Esta estadística es una suma de otras estadísticas, no se puede modificar manualmente.")
                        tools.enterParaContinuar()
                        tools.limpiarTerminal()

                    case 6:
                        #hp
                        while True:
                            try:    
                                new_stat=int(input(f'Introduce el nuevo valor para {menuModificacion[opcion]}: '))
                                if new_stat<1000:
                                    break
                            except ValueError:
                                print('Introduce un valor válido')

                        lista_dicts[indice_lista]['HP']=str(new_stat)
                    
                        lista_dicts[indice_lista]['Total']=int(lista_dicts[indice_lista]['HP'])+int(lista_dicts[indice_lista]['Attack'])+int(lista_dicts[indice_lista]['Defense'])+int(lista_dicts[indice_lista]['Sp. Atk'])+int(lista_dicts[indice_lista]['Sp. Def'])+int(lista_dicts[indice_lista]['Speed'])
                        lista_dicts[indice_lista]['Total']=str(lista_dicts[indice_lista]['Total'])
                        print("Cambio efectuado.")
                        tools.enterParaContinuar()

                    case 7:
                        #attack
                        while True:
                            try:    
                                new_stat=int(input(f'Introduce el nuevo valor para {menuModificacion[opcion]}: '))
                                if new_stat<1000:
                                    break
                            except ValueError:
                                print('Introduce un valor válido')

                        lista_dicts[indice_lista]['Attack']=str(new_stat)
                    
                        lista_dicts[indice_lista]['Total']=int(lista_dicts[indice_lista]['HP'])+int(lista_dicts[indice_lista]['Attack'])+int(lista_dicts[indice_lista]['Defense'])+int(lista_dicts[indice_lista]['Sp. Atk'])+int(lista_dicts[indice_lista]['Sp. Def'])+int(lista_dicts[indice_lista]['Speed'])
                        lista_dicts[indice_lista]['Total']=str(lista_dicts[indice_lista]['Total'])
                        print("Cambio efectuado.")
                        tools.enterParaContinuar()
                        
                    case 8:
                        #defense
                        while True:
                            try:    
                                new_stat=int(input(f'Introduce el nuevo valor para {menuModificacion[opcion]}: '))
                                if new_stat<1000:
                                    break
                            except ValueError:
                                print('Introduce un valor válido')

                        lista_dicts[indice_lista]['Defense']=str(new_stat)
                    
                        lista_dicts[indice_lista]['Total']=int(lista_dicts[indice_lista]['HP'])+int(lista_dicts[indice_lista]['Attack'])+int(lista_dicts[indice_lista]['Defense'])+int(lista_dicts[indice_lista]['Sp. Atk'])+int(lista_dicts[indice_lista]['Sp. Def'])+int(lista_dicts[indice_lista]['Speed'])
                        lista_dicts[indice_lista]['Total']=str(lista_dicts[indice_lista]['Total'])
                        print("Cambio efectuado.")
                        tools.enterParaContinuar()

                    case 9:
                        #sp atk
                        while True:
                            try:    
                                new_stat=int(input(f'Introduce el nuevo valor para {menuModificacion[opcion]}: '))
                                if new_stat<1000:
                                    break
                            except ValueError:
                                print('Introduce un valor válido')

                        lista_dicts[indice_lista]['Sp. Atk']=str(new_stat)
                    
                        lista_dicts[indice_lista]['Total']=int(lista_dicts[indice_lista]['HP'])+int(lista_dicts[indice_lista]['Attack'])+int(lista_dicts[indice_lista]['Defense'])+int(lista_dicts[indice_lista]['Sp. Atk'])+int(lista_dicts[indice_lista]['Sp. Def'])+int(lista_dicts[indice_lista]['Speed'])
                        lista_dicts[indice_lista]['Total']=str(lista_dicts[indice_lista]['Total'])
                        print("Cambio efectuado.")
                        tools.enterParaContinuar()

                    case 10:
                        #sp. def
                        while True:
                            try:    
                                new_stat=int(input(f'Introduce el nuevo valor para {menuModificacion[opcion]}: '))
                                if new_stat<1000:
                                    break
                            except ValueError:
                                print('Introduce un valor válido')

                        lista_dicts[indice_lista]['Sp. Def']=str(new_stat)
                    
                        lista_dicts[indice_lista]['Total']=int(lista_dicts[indice_lista]['HP'])+int(lista_dicts[indice_lista]['Attack'])+int(lista_dicts[indice_lista]['Defense'])+int(lista_dicts[indice_lista]['Sp. Atk'])+int(lista_dicts[indice_lista]['Sp. Def'])+int(lista_dicts[indice_lista]['Speed'])
                        lista_dicts[indice_lista]['Total']=str(lista_dicts[indice_lista]['Total'])
                        print("Cambio efectuado.")
                        tools.enterParaContinuar()

                    case 11:
                        #speed
                        while True:
                            try:    
                                new_stat=int(input(f'Introduce el nuevo valor para {menuModificacion[opcion]}: '))
                                if new_stat<1000:
                                    break
                            except ValueError:
                                print('Introduce un valor válido')

                        lista_dicts[indice_lista]['Speed']=str(new_stat)
                    
                        lista_dicts[indice_lista]['Total']=int(lista_dicts[indice_lista]['HP'])+int(lista_dicts[indice_lista]['Attack'])+int(lista_dicts[indice_lista]['Defense'])+int(lista_dicts[indice_lista]['Sp. Atk'])+int(lista_dicts[indice_lista]['Sp. Def'])+int(lista_dicts[indice_lista]['Speed'])
                        lista_dicts[indice_lista]['Total']=str(lista_dicts[indice_lista]['Total'])
                        print("Cambio efectuado.")
                        tools.enterParaContinuar()
                    
                    case 12:
                        #gen
                        while True:
                            try:
                                new_gen=int(input(f'Introduce la nueva generación para el Pokémon: '))
                                if new_gen<1000:
                                    break
                            except ValueError:
                                print("Introduce un valor válido.")
                            lista_dicts[indice_lista['Generation']]=str(new_gen)
                            print("Cambio efectuado")
                            tools.enterParaContinuar()
                            tools.limpiarTerminal()
                        lista_dicts[indice_lista]['Generation']=str(new_gen)
                    
                    case 13:
                        #legendario
                        while True:
                            try:
                                legendary=str(input(f'Quieres que este Pokémon sea legendario? (y/n) '))
                                if legendary=='y':
                                    lista_dicts[indice_lista]['Legendary']='True'
                                    break
                                elif legendary=='n':
                                    lista_dicts[indice_lista]['Legendary']='False'
                                    break
                                else:
                                    print("Introduce y o n.")
                                    tools.enterParaContinuar()
                                    tools.limpiarTerminal()
                            except ValueError:
                                print("Introduce n o y.")
                                tools.enterParaContinuar()
                                tools.limpiarTerminal()

            case 3:
                #generar estadísticas.
                menuEstadisticas={}
                contador=1
                for x in lista_dicts[0].keys():
                    menuEstadisticas[contador]=x
                    contador+=1
                

                tools.limpiarTerminal()
                print("De qué elemento quieres generar estadísticas?")
                opcion=tools.menuUniversal(menuEstadisticas)
                estadistca_elegida=menuEstadisticas[opcion]

                match opcion:
                    case 3 | 4 | 12 | 13:

                        tools.limpiarTerminal()
                        estadistica=tools.generarValoresEstadistica(lista_dicts, estadistca_elegida)
                        tools.calcularEstadisticaNum(estadistica, 'estadisticas.txt')
                        print("Se ha generado un fichero estadisticas.txt con la información.")
                        tools.enterParaContinuar()

                    case 5 | 6 | 7 | 8 | 9 | 10 | 11:

                        tools.limpiarTerminal()
                        valores_totales=tools.generarEstadisticaCombate(lista_dicts, estadistca_elegida)
                        tools.calcularEstadisticaCombate(valores_totales, 'battlestats.txt')
                        print("Se ha generado un fichero battlestats.txt con la información.")
                        tools.enterParaContinuar()
                        
            case 4:
                #guardar datos
                tools.desarDadesFitxer(lista_dicts, 'pokemon.txt')
            
            case 5:
                break

if __name__=='__main__':
    main()