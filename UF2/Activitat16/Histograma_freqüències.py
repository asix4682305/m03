#! /usr/bin/python3
#Noah Martos Teruel
#Escola del Treball 1HISX
#
#Definició: Programa que rep el nom d'un fitxer de text
#com argument, llegeix el seu contingut, i genera un
#histograma horitzontal amb la freqüència de les longituds
#de les paraules del text.

'''
1-> contar las letras de cada palabra
2-> por cada numero de letras igual, un contador (frecuencia)
3-> crear histograma horinzontal.
'''
import sys

def count_letter_from_words(word):
    """Compta les lletres d'un string

    Args:
        word (string): qualsevol string

    Returns:
        int: nombre de lletres a l'string.
    """
    return len(word)

def word_lenght_frequency_on_text(text):
    """Rep un text i crea una llista amb la frecuencia de la longitud
    de les paraules que conformen el text.

    Args:
        text (text): Qualsevol text que no sigui buit.

    Returns:
        frecuencia: Llista amb la frecuencia de la longitud de les paraules
        del text, sent el primer element el nombre de paraules amb una lletra,
        el segon, el nombre de paraules amb dues lletres, etc.
    """
    fichero=open(text)
    palabras=fichero.read().split()
    longitud=[]
    for palabra in palabras:
        longitud.append(count_letter_from_words(palabra))
    longitud.sort()
    max_value=max(longitud)
    frecuencia=[0]*(max_value + 1)
    for long in longitud:
        frecuencia[long]+=1
    frecuencia = frecuencia[1:]
    return frecuencia

def histograma_horizontal(frecuencia, caracter):
    """Donada una frecuencia i un caràcter, s'imprimeix el histograma
    horitzontal.

    Args:
        frecuencia (array): Llista que contingui les frecuencies que es volen
        representar a l'histograma.
        caracter (string): Caràcter utilitzat per a representar l'histograma

    Returns:
        print: Histograma horitzontal mostrat en el format:
        1: ###
        2: #
        3: ####
        El caràcter que apareix (#) es escollit per l'usuari.
    """
    comptador_linea=1
    for f in frecuencia:
        print(f"{comptador_linea}:", caracter*f)
        comptador_linea+=1
    return 0

def main():
    if len(sys.argv) != 3:
        print("ERR! n args incorrecte")
        print("Usage:", sys.argv[0], "text caracter")
        sys.exit(1)
    elif len(sys.argv[2]) != 1:
        print("ERR! el caràcter ha de ser d'un sol caràcter.")
        sys.exit(2)

    histograma_horizontal(word_lenght_frequency_on_text(sys.argv[1]), sys.argv[2])
    

if __name__ == '__main__':
    main()