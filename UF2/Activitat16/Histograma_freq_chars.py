#! /usr/bin/python3
#Noah Martos Teruel
#Escola del Treball 1HISX
#
#Definició: Programa que rep el nom d'un fitxer de text
#com argument, llegeix el seu contingut, i genera un
#histograma horitzontal amb la freqüència de les longituds
#de les paraules del text.

'''
1-> contar las letras de cada palabra
2-> por cada numero de letras igual, un contador (frecuencia)
3-> crear histograma horinzontal.
'''
import sys

def count_letter_from_words(word):
    """Compta les lletres que conté una paraula.

    Args:
        word (string): Una paraula.

    Returns:
        int: Longitud de la paraula.
    """
    return len(word)

def char_frequency_on_text(text, chars):
    """Donat un text i uns caràcters a buscar, dona una llista amb els
    caràcters buscats al text.

    Args:
        text (texto): text on buscar els caràcters.
        chars (string): caràcters a buscar.

    Returns:
        match (array): llista ordenada amb els caràcters buscats al text.
        caracter a buscar -> e a
        text -> buenas tardes a todos.
        match -> ["a", "a", "a", "e", "e"]
    """
    fichero=open(text)
    palabras=fichero.read().split()

    chars_in_text=[]
    for palabra in palabras:
        for p in palabra:
            chars_in_text.append(p)
    
    chars_in_text=sorted(chars_in_text)

    match=[]
    for caracter in chars_in_text:
        if caracter in chars:
            match.append(caracter)

    return match

def histograma_horizontal_chars(match):
    """Donada una llista amb caràcters, s'imprimeix un histograma horitzontal
    on es mostra la freqüència d'aquests caràcters a la llista.
    
    match-> ["a", "a", "a", "e", "e"]
    histograma:
        a: ###
        e: ##

    Args:
        match (array): Llista amb caràcters.

    Returns:
        print: histograma horitzontal mostrant la freqüència dels caràcters.
    """
    # Ordenamos la lista para facilitar la agrupación de caracteres iguales.
    match.sort()
    
    # Inicializamos variables para el primer carácter y su frecuencia.
    current_char = match[0]
    char_count = 1

    # Iteramos a partir del segundo elemento.
    for char in match[1:]:
        if char == current_char:
            # Si el carácter es el mismo que el anterior, incrementamos la cuenta.
            char_count += 1
        else:
            # Si encontramos un nuevo carácter, imprimimos el anterior y reiniciamos el contador.
            print(current_char, ":", "#" * char_count)
            current_char = char
            char_count = 1

    # No olvides imprimir el último carácter después de salir del bucle.
    print(current_char, ":", "#" * char_count)
    return 0

def main():
    if len(sys.argv) == 2:
        print("ERR! n args incorrecte")
        print("Usage:", sys.argv[0], "text caracter")
        sys.exit(1)
    elif len(sys.argv[2]) != 1:
        print("ERR! el caràcter ha de ser d'un sol caràcter.")
        sys.exit(2)
    chars_in_text=char_frequency_on_text(sys.argv[1], sys.argv[2:])
    histograma_horizontal_chars(chars_in_text)
    #histograma_horizontal(word_lenght_frequency_on_text(sys.argv[1]), sys.argv[2])
    

if __name__ == '__main__':
    main()