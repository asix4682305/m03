# !/usr/bin/python
#-*- coding: utf-8-*-
#Noah Martos Teruel
#Escola del Treball, 1 HISX
#
#Descripció: Programa que rep diversos arguments, distingeix
#els que representen una dada numèrica dels que representen una
#cadena de text, crea un array de floats i un altre de cadenes,
# i mostra el màxim valor de cada un d'aquests arrays.

#¿Què és el màxim d'un array de cadenes?
#El màxim d'un array de cadenes és aquella cadena que en ordre d'esquerra a dreta
#poseeix els caràcters amb valor ascii majors.

#Especificació d'entrada: n arguments, poden ser numèrics o strings.

import sys
import profe_example_function as tools

#separate args in two arrays: numbers and str.
numeric_array=[]
string_array=[]

def separate_arguments(arguments):
    """Separa arguments segons si son strings o numèrics.

    Args:
        arguments (string/numeric): Arguments strings o numèrics.

    Returns:
        array: Llista per valors numèrics i llista per valors string.
    """
    for arg in arguments:
        if tools.is_number(arg) == True:
            numeric_array.append(arg)
        else:
            string_array.append(arg)
    return 0

#Identify max value in each array.

def array_max_value(array):
    """Dona el valor màxim dins d'una array, i si es buida ho notifica.

    Args:
        array (llista): llista que conté qualsevol tipus de dades.

    Returns:
        string: El valor més gran de cada llista.
    """
    if len(array)>0:
        return max(array)
    else:
        return "The array is empty"

def main():

    if len(sys.argv) == 1:
        print("ERR! nombre d'arguments invàlid")
        print(f"Usage: {sys.argv[0]} hola 1234 adios 4321 ...")
        sys.exit(1)

    array_arguments = sys.argv[1:]
    separate_arguments(array_arguments)
    
    separate_arguments(array_arguments)
    #print(numeric_array)
    #print(string_array)
    print("Valor màxim numèric:", (array_max_value(numeric_array)))
    print("Valor màxim string:",array_max_value(string_array))
    return 0

if __name__ == '__main__':
    main()