def is_number(str):
    '''
    CHecks if str is a number, including negatives and float numbers.
    input: the string to be checked
    output: true if a number, false otherwise
    '''

    return str.lstrip("-+").replace(".", "", 1).isdigit()
    #lstrip: removes any leading characters.
        #syntax: characters (optional), if not, it removes spaces.
    #replace: replaces a specified phrase with another specifies phrase.
        #syntax: (oldvalue, newvalue, [count])
            #oldvalue-> to change
            #newvalue-> new value
            #count-> how many occurrences (default is all)
    #isdigit: return True if all the characters are digits, otherwise False.
        #syntax: string.isdigit()

def main():
    print("Positive rseults:")
    print(is_number("56423"))
    print(is_number("-543"))
    print(is_number("564.34"))
    print(is_number("-76.7"))

    print("\n Negative results:")
    print(is_number("hello"))
    print(is_number("5.45.67"))

#esto es un test driver, para no tener que hacerlo en el codigo
#se puede ejecutar con la siguiente función:

if __name__ == '__main__':
    main()