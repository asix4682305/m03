# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-21 
#
# Versió: 
#
# Descripció: 
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1
#
#       Execució 2

#Creamos una función que calcule primos:
import math
import sys

def is_prime(n):    #Definimos funcion
    '''
    Checks if a number is prime or not
    input: integer
    output: boolean
    '''
    divisor= 2  #Primer divisor usable
    number_is_prime= True   #Flag

    while (divisor <= math.sqrt(n)) and number_is_prime:    #mientras el divisor > a
                                                    #la mitad de n y sea primo.
        if n % divisor == 0:        #Si n entre el divisor =0...
            number_is_prime = False     #No es primo, exit
        divisor +=1 #Sumamos uno al divisor

    return number_is_prime  #Fi de la funció, es retorna al programa principal

limit= int(sys.argv[1]) #El limit es el input

for number in range(2, limit):  #En el rango del 2, al limite
    result=is_prime(number) #El resultado de hacer la funcion que hemos creado antes
    if result:  #Si el resultado es true
        print(f"{number}", end=" ")     #Print numero y separalo por un espacio

print("\n")