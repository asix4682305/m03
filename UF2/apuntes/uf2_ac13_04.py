# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-21 
#
# Versió: 
#
# Descripció: 
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1
#
#       Execució 2

import math

def is_prime(n):
    '''
    Checks if a number is prime or not.
    input: integer
    output: boolean
    '''

    #loop through all the numbers smaller than n
    for i in range(2, int(math.sqrt(n))+1):
        #if it is a divisor
        if n % i == 0:
            #the number is not prime
            return False
        return True