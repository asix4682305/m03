import math     #Funciones de matematicas

print("Let's look for prime number under a certain limit\n")
limit = int(input("Enter the upper limit: "))       #input limit

for n in range(2, limit):   #bucle hasta que se llegue al limite
    divisor = 2     #Cremos divisor con n 2 pq es el primer divisor usable.
    is_prime = True #Boolean

    while (divisor<=math.sqrt(n)) and is_prime: #Mientras que el divisor sea menor
                                #a la raiz cuadrada y is_prime sea False.
        if n % divisor ==0: #Si el numero entre el divisor es menor a 0...
            is_prime = False    #El numero no es prim, sortim a la següent interació
        divisor +=1 #Sumem 1 al divisor

    if is_prime:    #Si es prim...
        print(f"{n} ", end="")
