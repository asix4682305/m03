# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noa Martos
# Data: 2024-02-21 
#
# Versió: 
#
# Descripció: 
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1
#
#       Execució 2

import sys

PERCENTAGE1=10
PERCENTAGE2=12
PERCENTAGE3=17

def increased_value(value, percentage):
    '''
    Increases a value by applying the percentage received.
    input: value(float), percentage (float)
    output: float
    '''
    return value + value * percentage / 100

salary1= int(sys.argv[1])
salary2= int(sys.argv[2])
salary3= int(sys.argv[3])

increased_salary1=increased_value(salary1, PERCENTAGE1)
increased_salary2=increased_value(salary2, PERCENTAGE2)
increased_salary3=increased_value(salary3, PERCENTAGE3)

print(f"The new salaries are: {increased_salary1:.2f}, {increased_salary2:.2f}," + 
      f"{increased_salary3:.2f}")