# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos Teruel
# Data: 2024-02-22 
#
# Versió: 
#
# Descripció: Definició que diu si n correspon a l'edat d'una persona major
# o menor d'edat 
#
# Especificacions d'entrada:
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1
#
#       Execució 2

import sys

def major_menor_edat(edat):
    '''
    Diu si una edat correspon a major o menor d'edat.
    input: int major que uno.
    output: string
    '''
    if edat < 0:
        print(f"Error! edat {edat} no vàlida")   
        print(f"Usage: {sys.argv[0]} edat (major que zero)")

    if edat>=18:
        print(f"{edat} és major d'edat.")    #Si edat es major o igual a 18 print major d'edat.
    
    else:
        print(f"{edat} és menor d'edat.") #Sino, print menor d'edat.

#Tests
major_menor_edat(15)
major_menor_edat(29)
major_menor_edat(-5)