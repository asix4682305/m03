# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos Teruel
# Data: 2024-02-27 
#
# Versió: 
#
# Descripció: Funció per a calcular els dies d'un any.
#
# Especificacions d'entrada: Un nombres sencer, any.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      2024                366
#
#       Execució 2      2022                365

def DiesAny(a):

    if a<0:
        print(f"Err! any {a} no valid.")
        print(f"'a' ha de ser major o igual a 0.")

    elif a % 4 == 0:
        print(f"El año {a} tiene 366 dias.")

    else:
        print(f"El año {a} tiene 365 dias.")

#Tests
DiesAny(2024)
DiesAny(2022)
DiesAny(400)
DiesAny(-5)