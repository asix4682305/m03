# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos Teruel
# Data: 2024-02-27 
#
# Versió: 
#
# Descripció: Passa de segons al format 'HH:MM:SS'
#
# Especificacions d'entrada: Un nombre sencer positiu
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      65                  0 1 5
#
#       Execució 2      3661                1 1 1


def NormalitzarSegons(s):
    '''
    Transforma segons en hores, minuts i segons.
    input: un nombre integer
    output: tres nombres integer
    '''

    if s < 0:
        print(f"El valor {s} ha de ser superior a 0.")
    
    else:
        h=s//3600
        m=s%3600//60
        s=s%60    

        print(f"{h}:{m}:{s}")

NormalitzarSegons(65)
NormalitzarSegons(3661)
NormalitzarSegons(-5)
