# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos Teruel
# Data: 2024-02-27 
#
# Versió: 
#
# Descripció: Llegeix una quantitat d'hores, minuts i segons
# i diu els segons totals.
#
# Especificacions d'entrada: Tres nombres sencers.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      0 1 5               65
#
#       Execució 2      1 1 1               3661

def SegonsTotals(h, m, s):
    '''
    Calcula els segons totals entre les hores, minuts i segons donats.
    input: tres nombres integers.
    ouput: un nombre integer.
    
    '''

    if (h or m or s)<0:
        print("Err! els valors h, m i s han de ser positius.")
    
    else:
        hores_segons=h*3600 #Conversió d'hores a segons.
    #print(hores_segons)

        minuts_segons=m*60 #Conversió de minuts a segons.
    #print(minuts_segons)
    
        segons_totals=hores_segons+minuts_segons+s #Suma de tots els segons.
        print(segons_totals)

#Tests
SegonsTotals(0, 1, 5)
SegonsTotals(1, 1, 1)
SegonsTotals(-5, 2, 4)
