# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos
# Data: 2024-02-22 
#
# Versió: 
#
# Descripció: Funció is_prime que funciona amb qualsevol nombre.
#
# Especificacions d'entrada: Nombre sencer
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      5                   True
#
#       Execució 2      9                   False

import math
import sys

def is_prime(n):
    '''
    Checks if a number is prime or not.
    input: integer
    output: boolean
    '''
    number_is_prime=True

    if n==0 or n==1:
        number_is_prime=False
    
    else:

        if n<0:
            n=-n

        divisor=2
        
        while (divisor<=math.sqrt(n)) and number_is_prime:
            if n % divisor == 0:
                number_is_prime=False
            divisor+=1
        
        return number_is_prime

def primes_up_to_limit(limit):
    '''
    Using function is_prime gives prime numbers until a given limit.
    input: integer
    output: string
    '''
    
    if limit>0:
        for number in range(2, limit):
            result= is_prime(number)
            if result:
                print(f"{number}", end=" ")

    if limit<0:
        for number in range(limit, -2):
            result= is_prime(number)
            if result:
                print(f"{number}", end=" ")

    if limit==1 or limit==-1 or limit==0:
        print(f"{limit} no és un nombre prim")


primes_up_to_limit(int(sys.argv[1]))