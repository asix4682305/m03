# !/usr/bin/python3
#-*- coding: utf-8-*-
#
# Escola del Treball de Barcelona
# Administració de Sistemes informàtics
# Curs 2023-24
#
# Autor: Noah Martos Teruel
# Data: 2024-02-22 
#
# Versió: 1
#
# Descripció: Programa que utiliza funcions importades per a que l'usuari pugui veure els nombres primers 
#positius o negatius abans d'un limit.
#
# Especificacions d'entrada: Nombre sencer.
#
# Joc de proves:
#                       Entrada             Sortida
#       Execució 1      10                  2, 3, 5, 7,
#
#       Execució 2      -10                 -2, -3, -5, -7


from Funcionisprime import *
import sys

limit=int(sys.argv[1])
primes_up_to_limit