Historia: Tu familia es pobre y quieres encontrar un tesoro escondido del que se habla
por toda la región para conseguir dinero.

Estadísticas de los personajes:
-Nombre
-Clase
-Descripción
-Salud
-Ataque
-Defensa
-Inventario
-Dinero?

Personajes:
Enemigos
-Opción de diálogo
-Opción de combate
-Opción de ataque sigiloso(daga)

Trolls(bosque)
Murciélagos(cueva)
Piratas(barco e isla)->diferentes tipos?

Aliados
-Requisito(ítem,minijuego?)
Hada->bosque
Pirata->prisión del pueblo

Ubicaciones:
-Nombre
-Descripción
-Personajes
-Peligro

Aldea(inicio)
Bosque->Primer enemigo
Cueva->dos cofres para elegir
Pueblo->prisión(aliado?), tiendas
Barco->viaje a la isla, pelea naval
Isla->tesoro

Ítems:
-Nombre
-Descripción
-Estadística
-Usos

Espada de madera
Espada de hierro
Daga silenciosa
Arco
Flechas->normales, encantadas
Antorcha
Armadura(+ defensa)
Collar(+ salud)
Chile(+ ataque)
Poción salud
Poción defensa
Poción ataque
**comida**
Aldea->bocadillo de tu madre(+ salud)
Bosque->Corazón de enemigo( +ataque, -salud)/(+ dinero)->cocinar?(+ataque, +salud)
Pueblo->hamburguesa, fruta, carne de troll, redbull
Barco->pescado(minijuego pesca)
Isla->ron(pirata), coco(palmera)

UbicacionesXítems:
Aldea->Espada de madera/collar(+salud?)
Bosque->Arco(de enemigo), flechas(de enemigo), corazón(de enemigo), palo de madera, comida
Cueva->antorcha(se puede crear), cofre+defensa/cofre+ataque, pista(tesoro)
Pueblo->
    -Tienda de armas: flechas, espada de metal, daga sigilosa
    -Tienda de pociones: +salud, +ataque, +defensa
    -Tienda de comida: comida varia
Barco->No se consigue ningún ítem
Isla->Tesoro