#! /usr/bin/python3
#Autor: Noah Martos Teruel
#Descripción: Un juego de aventuras por texto. En esta aventura entra en juego la estrategia y
#las probabilidades. La idea del juego es mucho más larga, pero por falta de tiempo, solo hay
#hasta el primer encuentro, de manera que se pueden ver todas las mecánicas principales del juego.
#De modo que esta es una especia de demo.

#Estos son los módulos importados necesarios para el juego.

import sys
import json
import os
import time
import random

##menu##

def CreacionDiccionarios(ruta_archivo, elemento):
    """Funcion que convierte elementos de ficheros json en diccionarios python.

    Args:
        ruta_archivo (string): localización del archivo json.
        elemento (string): elemento que convertir en diccionario.

    Returns:
        diccionario: diccionario de python.
    """
    with open(ruta_archivo, "r") as archivo:
        datos_json = json.load(archivo)
    
    diccionario=datos_json[elemento]
    return diccionario

def menuUniversal(menu, sleep=0):
    """Función que muestra datos con apariencia de menú y deja al usuario
    elegir una opción dentro de las existentes usando números.

    Args:
        menu (diccionario): diccionario de python con las opciones a mostrar.
        sleep (num): tiempo que pasa entre que aparece cada entrada del diccionario.

    Returns:
        opcion: opcion que ha elegido el usuario.
    """
    opciones=len(menu)

    while True:
        try:
            for x, y in menu.items():   #Display menu
                print(x, y)
                time.sleep(sleep)

            opcion=int(input('Introduce un número: '))
            if opcion <= opciones and opcion > 0:
                return opcion
            else:
                limpiarTerminal()
        except ValueError:
            print('Introduce un número de los indicados en pantalla.')
            os.system('clear')

def limpiarTerminal(sleep=0):
    """Función que limpia la terminal.

    Args:
        sleep (num): Tiempo que tarda en limpiarse la terminal.
    """
    time.sleep(sleep)
    os.system('clear')

def escribirNombre(rol):
    """Función que hace que el usuario escriba un nombre y se asegura de que este sea adecuado.

    Args:
        rol (string): para especificar a qué se le pone el nombre.

    Returns:
        nombre: nombre válido introducido por el usuario.
    """
    while True:
        try:
            nombre=str(input(f"Escribe un nombre para tu {rol}: "))
            if len(nombre)> 10:
                print('Demasiado largo.')
                time.sleep(2)
                limpiarTerminal()
            else:
                return nombre
        except:
            print('Nombre inválido.')

def enterParaContinuar():
    """Espera a que el usuario le de a Enter para romper el bucle y continuar con el flujo.
    """
    while True:
        input("Pulsa Enter para continuar: ")
        break

def mostrarTexto(ruta, elemento, valor, diccionario=None, sleep=0):
    """Muestra el texto ubicado en un archivo json separando las líneas por "\n".

    Args:
        ruta (string): ubicación del fichero json del que se quiere sacar el texto.
        elemento (string): de los diccionarios existentes en el fichero, de cuál se quiere sacar la información.
        valor (string): del diccionario seleccionado, qué entrada de texto es la deseada.
        diccionario (dictionary, optional): Si hay palabras que quieras substituir por otras, introduce un diccionario 
            donde la clave se encuentre escrita en el texto y sea sustituida por su valor correspondiente. Defaults to None.
        sleep (int, optional): Tiempo de espera entre el print de cada línea. Defaults to 0.
    """
    with open(ruta, "r") as archivo:
        datos_json=json.load(archivo)

        texto=datos_json[elemento][valor]
        
        if diccionario==None:
            for linea in texto.split('\n'):
                print(linea)
                time.sleep(sleep)

        else:
            for linea in texto.split("\n"):
                for clave in diccionario:
                    if clave in linea:
                        print(linea.replace(clave, diccionario[clave]))
                        time.sleep(sleep)
                    else:
                        print(linea)
                        time.sleep(sleep)

def mostrarDialogo(ruta, elemento, valor, character, diccionario=None):
    """Parecido a mostrarTexto, con la diferencia de que cuando se printea la línea, se muestra antes
    el nombre introducido en character. De este modo puede crearse un diálogo dejando claro quién dice la
    información.

    Args:
        ruta (string): ubicación del fichero json donde se encuentran el/los dialogos.
        elemento (string): de los diccionarios dentro del fichero de 'ruta', donde está el diálogo deseado.
        valor (string): dentro del diccionario 'elemento', cuál es el valor que se desea mostrar.
        character (string): personaje que dice la frase.
        diccionario (diccioinario, optional): Si hay palabras que quieres que sean sustituidas por otras, introduce un
            diccionario donde las claves se encuentran en el texto, para que sean sustituidas por su valor corrspondiente
            .Defaults to None.
    """
    with open(ruta, "r") as archivo:
        datos_json=json.load(archivo)

        texto=datos_json[elemento][valor]

        if diccionario==None:
            for linea in texto.split("\n"):
                print(f"{character}: {linea}")
        else:
            for linea in texto.split("\n"):
                linea_modificada=linea
                for clave in diccionario:
                    if clave in linea_modificada:
                        linea_modificada=linea_modificada.replace(clave, diccionario[clave])
                print(f"{character}: {linea_modificada}")                   

def functionInventario(inventario, min=0):
    """Parecido a menuUniversal, pero si solo hay 'min' elementos, dice que no tienes nada en el inventario.  

    Args:
        inventario (diccionario): diccionario que funciona como inventario.
        min (int): Cantidad mínima de entradas en el diccionario para printear que no hay ítems. Defaults to 0.

    Returns:
        none: sale de la función si no hay nada en el diccionario.
    """
    opciones=len(inventario)
    
    if opciones==min:
        print("No tienes nada en el inventario.")
        return

    while True:
        try:
            for x, y in inventario.items():   #Display menu
                print(x, y)

            opcion=int(input('Introduce un número: '))
            if opcion <= opciones and opcion > 0:
                return opcion
        except ValueError:
            print('Introduce un número de los indicados en pantalla.')

def mostrarEstadisticas(stats, diccionario=None):
    """Printea las claves claves y los valores respectivos de un diccionario.

    Args:
        stats (diccionario): Diccionario donde se encuentran las estadísticas.
        diccionario (diccionario, optional): Si quieres que algunas palabras sean sustituidas por otros valores, introduce
            un diccionario con claves (valores a sustituir) y sus valores respectivos. Defaults to None.
    """
    for x,y in stats.items():
        for clave in diccionario:
            if y == clave:
                y = diccionario[clave]
        print(f"{x}: {y}")
    
def cambio_estadistica(objeto, character):
    """Esta función busca la clave 'estadística' dentro del diccionario de objeto para encontrar qué estadística del
    character hay que cambiar (la estadística debe existir en el diccionario character). Después busca en el diccionario
    objeto cuánto debe cambiar esta estadística, y ese número se lo suma a la estadística correspondiente del character.
    Además, busca la clave 'Usos' en el diccionario objeto y le resta un número.

    Args:
        objeto (diccionario): Objeto que se desea usar, debe tener las claves "Estadística" (ej. ataque), entonces debe tener
            una clave por esa misma estadística (en este caso "ataque") con un valor numérico (ej. 10). También debe tener la clave usos,
            (ej. 3).
        character (diccionario): Personaje sobre el que se utiliza el ítem. Debe tener una clave igual a la estadística que se va a cambiar.
            En este caso debe tener la clave "Ataque".

    Returns:
        objeto, character: devuelve los diccionarios introducidos para que puedas guardar las nuevas estadísticas en otro o el mismo diccionario.
    """
    estadistica_a_cambiar=objeto["Estadística"]
    cambio=objeto[estadistica_a_cambiar]
    objeto["Usos"]=objeto["Usos"]-1
    character[estadistica_a_cambiar]=character[estadistica_a_cambiar]+cambio
    return objeto, character

def main():
    ###ficheros necesarios###

    #Estas son las rutas donde se encuentran los diccionarios usados.
    menus="./Datos/menus.json"
    characters="./Datos/characters.json"
    texts="./Datos/textos.json"
    dialogos="./Datos/dialogos.json"
    ubicaciones="./Datos/localizaciones.json"
    items="./Datos/items.json"

    ###variables necesarias###
    diccionario={}  #diccionario para sustituir claves por valores introducidos por el jugador.
    SLEEP=0.65  #tiempo de espera en el printeo de las líneas.
    inventario=CreacionDiccionarios(menus, "menuInventario")    #inventario del jugador.
    asociacion_objetos_inventario={}    #diccionario necesario para asociar el diccionario inventario con las características de cada ítem.

    ##codigo##

    ##menu principal##
    limpiarTerminal(SLEEP)

    menu_principal=CreacionDiccionarios(menus, "menuPrincipal") #Mostramos el menú principal
    opcion=menuUniversal(menu_principal, SLEEP) #Guardamos la opción elegida por el jugador.
    
    #Si el jugador decide salir de la partida.
    if opcion == 2:
        print("¡Hasta pronto!")
        sys.exit()
    
    ##menu nombre##
    limpiarTerminal(SLEEP)

    print("Elige un nombre para tu protagonista: ") #Mostramos menú de elección de nombre
    time.sleep(SLEEP)

    menuNombre=CreacionDiccionarios(menus, "menuNombre") #Creación del menú para escoger nombre.
    menuNombre['1']='Noesi' #Opción default
    opcion=menuUniversal(menuNombre, SLEEP) #guardamos la elección del jugador.

    if opcion == 1:
        diccionario['nombre']=menuNombre['1']   #Guardamos en el diccionario la elección.
        limpiarTerminal(SLEEP)

    else:
        limpiarTerminal(SLEEP)
        diccionario['nombre']=escribirNombre('protagonista') #Guardamos en el diccionario el nombre escrito por el jugador.
        limpiarTerminal(SLEEP)

    print(f"Tu nombre es {diccionario['nombre']}")  #Informamos al jugador de cuál es su nombre.
    time.sleep(SLEEP)

##inicio historia##
    mostrarTexto(texts, 'historia', 'historiaInicial', diccionario, SLEEP)  #Mostramos el texto introductorio a la historia.
    enterParaContinuar()
    limpiarTerminal()

    mostrarTexto(texts, 'historia', 'nuevoDia')
    menuDespertar=CreacionDiccionarios(menus, "menuDespertar") #Mostramos menú para decidir si despertarse o no.
    
    counter=0
    while True:
        print("¿Qué quieres hacer?")
        time.sleep(SLEEP)
        opcion=menuUniversal(menuDespertar, SLEEP)

        if opcion==1:   #Si el jugador quiere despertarse, se rompe el bucle
            limpiarTerminal()
            break

        elif opcion==2: #Si el jugador quiere seguir durmiendo, sigue el bucle
            counter+=1
            limpiarTerminal(SLEEP)

        if counter==3:  #Si el jugador quiere seguir durmiendo 3 veces, es despertado y se rompe el bucle
            mostrarDialogo(texts, "Mamá", "despiertaYa", "Mamá", diccionario)
            enterParaContinuar()
            limpiarTerminal()
            break
    
    ###hermano###
    limpiarTerminal()
    mostrarTexto(texts, 'historia', 'hermano1') #El jugador se encuentra a su hermano

    print("¿Cómo se llama tu hermano?")
    time.sleep(SLEEP)

    menuHermano=CreacionDiccionarios(menus, "menuNombre")   #Menú para elegir el nombre del hermano.
    menuHermano['1']='Miguel'   #Nombre por defecto

    opcion=menuUniversal(menuHermano, SLEEP) #Mostrar menú para elegir el nombre.
    
    if opcion==1:   #Opción default
        diccionario['Hermano']=menuHermano['1'] #Se guarda el nombre en el diccionario.

    else:   #Opción escribir nombre
        diccionario['Hermano']=escribirNombre('hermano') #Se guarda el nombre escrito por el jugador en el diccionario.

    limpiarTerminal()       
    
    
    menuHablar=CreacionDiccionarios(menus, "menuHablar")    #Creación diccionario para tener interacciones con personajes amigables.
    
    hablado=False   #Inicializar variable para verificar si se ha hablado con el hermano.

    while True:
        print("¿Qué quieres hacer?")
        time.sleep(SLEEP)

        opcion=menuUniversal(menuHablar, SLEEP)
        limpiarTerminal(SLEEP)

        match opcion:
            case 1:
                #Opción hablar
                mostrarDialogo(texts, "Hermano", "hermanoHablar1", diccionario['Hermano'], diccionario) #Mostrar diálogo hermano.

                menuDialogo=CreacionDiccionarios(dialogos, "hermano1") #Cración menú para responder a hermano.
                enterParaContinuar()
                limpiarTerminal(SLEEP)

                menuUniversal(menuDialogo, SLEEP)   #Uso menú para responder a hermano.
                limpiarTerminal(SLEEP)
                hablado=True    #Cambiar valor flag, ya puedes irte si usas la opción irse

                mostrarDialogo(texts, "Hermano", "hermanoHablar2", diccionario['Hermano'], diccionario) #Mostrar diálogo hermano.

                menuDialogo=CreacionDiccionarios(dialogos, "hermano2") #Creación menú para seguir hablando a hermano o no.
                enterParaContinuar()
                limpiarTerminal()

                option=menuUniversal(menuDialogo, SLEEP)    #Uso menú para seguir hablando con hermano o no.
                limpiarTerminal(SLEEP)

                if option==1:
                    #Opción seguir hablando
                    mostrarDialogo(texts, "Hermano", "hermanoHablar3", diccionario['Hermano'], diccionario) #Mostrar diálogo hermano.
                    enterParaContinuar()
                    limpiarTerminal(SLEEP)
            case 2:
                #Opción hacer regalo
                print("Inventario:")
                functionInventario(inventario, 1)   #Llamada a función inventario, no se puede hacer regalo porque no tienes nada.
                enterParaContinuar()
                limpiarTerminal(SLEEP)

            case 3:
                #Opción atacar
                mostrarDialogo(texts, "Mamá", "nolePegues", "Mamá", diccionario)    #Tu madre te dice que no le pegues.
                enterParaContinuar()
                limpiarTerminal(SLEEP)

            case 4:
                #Opción ver estadísticas.
                estadisticas=CreacionDiccionarios(characters, "hermano")    #Creación menú estadísticas hermano
                mostrarEstadisticas(estadisticas, diccionario)  #Display de las estadísticas
                enterParaContinuar()
                limpiarTerminal(SLEEP)
            
            case 5:
                #Opción irse
                if hablado==True: #Si ya has hablado con el hermano,
                    break   #irse
                
                else: #Si no has hablado con el hermano,
                    print("Debería hablar con mi hermano.") #Notificar de que hay que hablar con el hermano, 
                    enterParaContinuar()    #repetir bucle.

    ###salir a la aldea###
    mostrarTexto(texts, "comentarios", "1", sleep=SLEEP)    #Mostrar texto para seguir con la historia.

    while True:
        print("Aldea Pacífica")
        print("¿Qué quieres hacer?")

        menuLocalizacion=CreacionDiccionarios(menus, "menuLocalización")    #Creación menú opciones al entrar en una localización
        opcion=menuUniversal(menuLocalizacion, sleep=SLEEP) #Menú opciones localización.

        if opcion==1:
            #Descripción
            limpiarTerminal()
            Datos_ubicacion=CreacionDiccionarios(ubicaciones, "Aldea Pacífica")#Crear menú descripción del lugar.
            
            print("Aldea Pacífica")
            mostrarEstadisticas(Datos_ubicacion, diccionario)   #Mostrar estadísticas del lugar.
            enterParaContinuar()
            limpiarTerminal()

        else:
            limpiarTerminal()
            mostrarTexto(texts, "comentarios", "2", sleep=SLEEP)    #Mostrar comentario
            enterParaContinuar()
            limpiarTerminal()            
            break   #Salir del bucle
    
    mostrarTexto(texts, "comentarios", "3") #Mostrar texto
    enterParaContinuar()
    limpiarTerminal()    

    mostrarDialogo(texts, "vecinoJaume", "1", "Vecino Jaume", diccionario)  #Mostrar dialogos con 'vecino jaume'
    mostrarDialogo(texts, "vecinoJaume", "2", "Vecino Juame", diccionario)

    dialogo_vecino=CreacionDiccionarios(dialogos, "vecino1")    #Creación menú para responder a vecino

    opcion=menuUniversal(dialogo_vecino, SLEEP) #Mostrar menú para responder a vecino

    #Las dos opciones tienen una respuesta del vecino diferente.
    if opcion==1:
        mostrarDialogo(texts, "vecinoJaume", "2.1", "Vecino Jaume", diccionario)
        time.sleep(SLEEP)
    else:
        mostrarDialogo(texts, "vecinoJaume", "2.2", "Vecino Jaume", diccionario)
        time.sleep(SLEEP)

    #Mostrar dialogos vecino.
    mostrarDialogo(texts, "vecinoJaume", "3", "Vecino Jaume", diccionario)
    time.sleep(SLEEP)
    mostrarDialogo(texts, "vecinoJaume", "3.1", "Vecino Jaume", diccionario)
    time.sleep(SLEEP)
    mostrarDialogo(texts, "vecinoJaume", "3.2", "Vecino Jaume", diccionario)
    time.sleep(SLEEP)
    mostrarDialogo(texts, "vecinoJaume", "3.3", "Vecino Jaume", diccionario)
    time.sleep(SLEEP)
    mostrarDialogo(texts, "vecinoJaume", "3.4", "Vecino Jaume", diccionario)
    time.sleep(SLEEP)
    enterParaContinuar()
    limpiarTerminal()

    #Recibes espada de madera
    inventario["1"]="Espada de madera"  #Añadir espada de madera al diccionario inventario.
    asociacion_objetos_inventario[inventario["1"]]="woodsword"  #Añadir espada de madera al menú de asociación con el ítem woodsword
    "print(espada_madera_stats)"
    
    "print(asociacion_objetos_inventario)"

    print("Has recibido", inventario["1"])  #Notificar al usuario de que ha recibido el ítem.
    enterParaContinuar()
    limpiarTerminal()

    mostrarTexto(texts, "comentarios", "4") #Mostrar dialogo salida villa.
    enterParaContinuar()
    limpiarTerminal()

    #Conversación con la madre antes de salir.
    mostrarDialogo(texts, "Mamá", "bocadillo", "Mamá", diccionario)
    time.sleep(SLEEP)
    mostrarDialogo(texts, "Mamá", "cuidado", "Mamá", diccionario)
    time.sleep(SLEEP)

    #Recibes bocadillo de nociyeah
    inventario["2"]="Bocadillo de NociYeah" #añadir bocadillo al diccionario inventario
    asociacion_objetos_inventario[inventario["2"]]="nociyeah"   #Añadir bocadillo al menú de asociación con el ítem nociyeah

    print("Has rebicido", inventario["2"])  #Notificar al usuario de que ha recibido el ítem
    enterParaContinuar()
    limpiarTerminal()

    print("Has salido de Aldea Pacífica.")  #Notificar al usuario de que ha salido de la aldea.
    enterParaContinuar()
    limpiarTerminal()

    ###bosque###
    mostrarTexto(texts, "historia", "encuentroTroll", sleep=SLEEP)  #Texto encuentro con el troll.
    mostrarTexto(texts, "comentarios", "5", sleep=SLEEP)

    print("¿Qué quieres hacer?")
    ataqueoSigilo=CreacionDiccionarios(menus, "AtaqueoSigilo")  #Decisión ataque o sigiloo.
    opcion=menuUniversal(ataqueoSigilo)
    
    escapado=False  #Inicializacion flag para ver si has escapado del troll
    match opcion:

        case 2:
            #Opción sigilo
            numero_aleatorio=random.randint(1,100)  #Generar número del 1 al 100

            if numero_aleatorio<=10:    #Si el número es menor o igual a 10 (10%), escapas
                print("¡Escapas sin problemas!")
                escapado=True   #flag escapado set to true
                enterParaContinuar

            else:   #Si no, no logras escapar y entras en combate igualmente
                print("¡No has logrado escapar!")
                enterParaContinuar()

    limpiarTerminal()

    if escapado==False: #Si no has logrado escapar
        estadisticas_troll=CreacionDiccionarios(characters, "troll")    #Creación estadísticas troll
        estadisticas_main=CreacionDiccionarios(characters, "main")  #Creación estadísticas main

        while True:
            atacado=False   #flag para decidir cuando ataca el troll

            print("¿Qué quieres hacer?")
            menuCombate=CreacionDiccionarios(menus, "menuCombate")  #Creación menú combate
            opcion=menuUniversal(menuCombate, SLEEP)    #Menú combate

            match opcion:
                case 1:
                    #hablar
                    atacado=True    #Después de esta interacción, el troll atacará
                    limpiarTerminal()
                    dialogo_troll=CreacionDiccionarios(dialogos, "troll")   #Mostrar dialogo troll
                    menuUniversal(dialogo_troll)    #Responder al troll (opción única)
                    
                    limpiarTerminal()
                    mostrarDialogo(texts, "troll", "1", "Troll")    #Troll responde
                    enterParaContinuar()
                    limpiarTerminal()

                    mostrarTexto(texts, "comentarios", "6", sleep=SLEEP)    #Mostrar comentario
                    enterParaContinuar()
                    limpiarTerminal()
                    
                case 2:
                    #ataque
                    atacado=True    #Después de esta interacción, el troll atacará
                    limpiarTerminal()

                    while True:
                        print("Ataque: ")
                        menuAtaque=CreacionDiccionarios(menus, "menuAtaque")
                        opcion2=menuUniversal(menuAtaque)   #Mostrar menú ataque
                        limpiarTerminal()

                        match opcion2:

                            case 1:
                                #Opción ataque normal
                                print("Intentas golpear al Troll.")
                                enterParaContinuar()

                                numero_aleatorio=random.randint(1,100)  #Generar número aleatorio entre 1 y 100
                                if numero_aleatorio<=80: #Si el número es más pequeño o igual a 80 (80%)
                                    print("Has conseguido golpear al Troll.")   #Consigues golpear al troll
                                    enterParaContinuar()
                                    limpiarTerminal()

                                    estadisticas_troll["Salud"]=estadisticas_troll["Salud"]-estadisticas_main["Ataque"] #La salud del troll disminuye tantos puntos como sea el ataque de main
                                    time.sleep(SLEEP)

                                    print("Troll:")
                                    mostrarEstadisticas(estadisticas_troll, diccionario) #Mostrar las estadísticas del troll ahora
                                    enterParaContinuar()
                                    limpiarTerminal()
                                    break

                                else:   #Si no
                                    print("No has conseguido golpear al Troll.")    #Notificar que no se ha conseguido golpear al troll
                                    enterParaContinuar()
                                    limpiarTerminal()
                                    break

                            case 2:
                                #items#
                                
                                limpiarTerminal()
                                opcion3=functionInventario(inventario, 1)   #Mostrar ítems

                                if opcion3==None: #Si no tienes más objetos
                                    print("No tienes más objetos.") #Notificar
                                    enterParaContinuar()
                                    limpiarTerminal()
                                    break   #Salir del menú

                                if opcion3==0:  #Opción salir
                                    break   #Salir del menú

                                else:   #Cualquier otra opción
                                    limpiarTerminal()

                                    inventario_objeto=inventario[str(opcion3)]  #Ítem equivalente al número escogido por el usuario.

                                    objeto_actual=CreacionDiccionarios(items, asociacion_objetos_inventario[inventario_objeto]) #Creación del diccionario de estadísticas del objeto.
            
                                    "print(main)"
                                    print(f"{inventario[str(opcion3)]}:")   #Mostrar estadísticas del objeto
                                    mostrarEstadisticas(objeto_actual, diccionario)
                                    enterParaContinuar()
                                    limpiarTerminal()
                                    
                                    print("\n¿Quieres usar", inventario[str(opcion3)], "?") #Preguntar al jugador si quiere usar el objeto

                                    menusiono=CreacionDiccionarios(menus, "MenuSioNo")  #Menú para decidir si quiere usar el objeto
                                    opcion4=menuUniversal(menusiono, SLEEP)

                                    if opcion4==1:
                                        #Opción sí
                                        objeto_actual, estadisticas_main=cambio_estadistica(objeto_actual, estadisticas_main) #cambio estadísticas objeto y personaje
                                        limpiarTerminal()

                                        if objeto_actual["Usos"]==0:    #Si el objeto se queda sin usos.
                                            limpiarTerminal()
                                            print(f"{inventario[str(opcion3)]} se ha roto.")    #Notificar que el objeto se ha roto
                                            enterParaContinuar()
                                            limpiarTerminal()

                                            del inventario[str(opcion3)]    #Eliminar objeto del inventario
                                            enterParaContinuar()
                                            limpiarTerminal()
                                        
                                        print("Estas son tus estadísticas ahora:")  #Mostrar nuevas estadísticas
                                        mostrarEstadisticas(estadisticas_main, diccionario)

                                        enterParaContinuar()
                                        limpiarTerminal()
                                        break
                            case 3:
                                break

                case 3:
                    #Estadísticas troll
                    limpiarTerminal()
                    mostrarEstadisticas(estadisticas_troll, diccionario)    #Mostrar estadísticas troll
                    enterParaContinuar()
                    limpiarTerminal()
                    

                case 4:
                    #Estadísticas main
                    limpiarTerminal()
                    mostrarEstadisticas(estadisticas_main, diccionario) #Mostrar estadísticas main
                    enterParaContinuar()
                    limpiarTerminal()
                    
            if estadisticas_troll["Salud"]<=0:  #Si la salud del troll es inferior o igual a cero
                print("Has vencido al troll.")  #Notificar que se ha vencido al troll
                break   #Salir del modo combate
            
            if atacado==True:   #Si ya has atacado o hablado con el troll
                print("El troll intenta atacar (50%)")  #Notificar que el troll intenta atacar
                enterParaContinuar()
                limpiarTerminal()

                numero_aleatorio=random.randint(1,100)  #Calcular número entre 1 y 100
                if numero_aleatorio<=50:    #Si el número es más pequeño o igual a 50 (50%)
                    print("El troll consigue atacarte.")    #Notificar que el troll te ha atacado
                    enterParaContinuar()
                    limpiarTerminal()

                    estadisticas_main["Salud"]=estadisticas_main["Salud"]-estadisticas_troll["Ataque"]  #Disminuir salud del main tantos puntos como el ataque del troll

                    print("Estas son tus estadísticas ahora:")  #Mostrar nuevas estadísticas
                    mostrarEstadisticas(estadisticas_main, diccionario)
                    enterParaContinuar()
                    limpiarTerminal()

                    print("Estas son las estadísticas del troll ahora:")    #Mostrar estadísticas del troll
                    mostrarEstadisticas(estadisticas_troll, diccionario)
                    enterParaContinuar()
                    limpiarTerminal()

                else:   #Si no
                    print("El troll no ha conseguido atacarte.")    #Notificar que el troll no te ha atacado
                    enterParaContinuar()
                    limpiarTerminal()

            if estadisticas_main["Salud"]<=0:   #Si la salud del main es menor o igual a cero
                print("Has perdido.")   #Notificar que has perdido
                sys.exit()  #Salir del juego
    
    print("Ahora que has entrado al bosque espera un largo camino por delante...")
    

if __name__ == '__main__':
    main()

