Sempre que n es major que cero, la funció es crida a si mateixa.

def countdown(n):
    print(n)
    if n>0:
        countdown(n-1)

def factorial(n):
    if n <=1:
        return 1
    else:
        n * factorial(n-1)