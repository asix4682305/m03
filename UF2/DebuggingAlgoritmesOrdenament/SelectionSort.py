#! /bin/bash
#Noah Martos Teruel
#Debugging mètode ordenament SelectionSort.
'''
El mètode d'ordenament per selecció consisteix en buscar el menor entre
tots els nombres no ordenats i colocar-ho al principi de la llista, després
s'ha de repetir el mateix amb els restants, sense comptar el ja ordenat.
Es com si en cada iteració la llista es fes més petita, reduint el nombre
d'iteracions.'''


from random import sample 

lista = list(range(100)) 

vectorselect = [9, 5, 2, 3, 1] 


def selectionsort(vectorselect):
    """Esta función ordenara el vector que le pases como argumento con el Método Selection Sort"""
    print ("El vector a ordenar es:",vectorselect)
    
    largo = 0 #Se crea variable para alojar longitud de la lista
    
    for _ in vectorselect:
        largo += 1 #Se asigna la longitud de la lista a "largo".
    print(largo)
        
    for i in range(largo): #Se itera una vez por cada elemento de la lista.
        minimo = i #Se escoge el número mínimo, que comenzaría siendo 0.
        print(minimo)
        for j in range(i+1, largo): #Iteramos por todos los numeros menos el seleccionado.
            print(j)
            if vectorselect[minimo] > vectorselect[j]: #Si el elemento seleccionado es más grande que cualquiera de los elementos siguientes, dejará de ser el mínimo.
                print(vectorselect[minimo], vectorselect[j])
                minimo = j #Y pasará a serlo el elemento con el que se comparaba.
                #El nuevo elemento mínimo terminará de iterar por el resto de elementos de la lista
                #con la misma condición, hasta que el mínimo al terminar la lista sea realmente el
                #número más pequeño.
                print(vectorselect)
        vectorselect[i], vectorselect[minimo] = vectorselect[minimo], vectorselect[i]
        #Cuando se tenga el número más pequeño de la lista actual, se cambiará el orden, colocando
        #el elementos mínimo donde se encontraba el primer elemento de la lista desordenada restante.
        #En la siguiente iteración, el número mínimo que se acaba de establecer, no será iterado, queda fijo.
        print(vectorselect)
    print("El vector ordenado es: ",vectorselect)

selectionsort(vectorselect)