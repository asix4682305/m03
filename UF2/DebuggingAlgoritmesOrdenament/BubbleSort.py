#! /bin/bash
#Noah Martos Teruel
#Debugging mètode ordenament bubblesort.
'''BubbleSort es un algoritme d'ordenament simple. Funciona revisant
cada element de la llista a ordenar amb el que segueix, canviat-los de
posició si estàn en un ordre incorrecte. És necessari repetir aquest procés
varies vegades fins que no es necessitin més canvis, cosa que significa
que la llista ha quedat ordenada.'''

from random import sample 
# Importamos un Método de la biblioteca random para generar listas aleatorias

lista = list(range(100)) # Creamos la lista base con números del 1 al 100

# Creamos una lista aleatoria con sample 
#(8 elementos aleatorios de la lista base)
vectorbs = sample(lista,8) 


def bubblesort(vectorbs):
    """Esta función ordenara el vector que le pases como argumento con el Método de Bubble Sort"""
    #Mostra la llista a ordenar.
    print("El vector a ordenar es:",vectorbs)
    n = 0
    
    for _ in vectorbs: #Variable con el valor de la longitud de la lista
        n += 1
    
    #print(n)

    for i in range(n-1): #Itera n veces (-1 porque el 0 cuenta)
        #print (i)
        for j in range(0, n-i-1): #Itera por todos los numeros menos i, estando un valor "i" fijo hasa que se termine de iterar.
            #print(j)
            if vectorbs[j] > vectorbs[j+1] : #Si el número actual és mes gran que el número següent,
                vectorbs[j], vectorbs[j+1] = vectorbs[j+1], vectorbs[j] #Hi canviem el ordre
            #print(vectorbs)
    print ("El vector ordenado es: ",vectorbs)

bubblesort(vectorbs)