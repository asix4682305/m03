#! /usr/bin/python3
#Noah Martos Teruel
#09-05-2024

import json
import time
import os

def CreacionDiccionarios(ruta_archivo=str, elemento=str):
    """Funcion que convierte elementos de ficheros json en diccionarios python.

    Args:
        ruta_archivo (string): localización del archivo json.
        elemento (string): elemento que convertir en diccionario.

    Returns:
        diccionario: diccionario de python.
    """
    with open(ruta_archivo, "r") as archivo:
        datos_json = json.load(archivo)
    
    diccionario=datos_json[elemento]
    return diccionario
    
def menuUniversal(menu=dict, sleep=0):


    """Función que muestra datos con apariencia de menú y deja al usuario
    elegir una opción dentro de las existentes usando números.

    Args:
        menu (diccionario): diccionario de python con las opciones a mostrar.
        sleep (num): tiempo que pasa entre que aparece cada entrada del diccionario.

    Returns:
        opcion: opcion que ha elegido el usuario.
    """
    opciones=len(menu)

    while True:
        try:
            for x, y in menu.items():   #Display menu
                print(x, y)
                time.sleep(sleep)

            opcion=int(input('Introduce un número: '))
            if opcion <= opciones and opcion > 0:
                return opcion                
        except ValueError:
            print('Introduce un número de los indicados en pantalla.')
            os.system('clear')

def limpiarTerminal(sleep=0):

    """Función que limpia la terminal.

    Args:
        sleep (num): Tiempo que tarda en limpiarse la terminal.
    """
    time.sleep(sleep)
    os.system('clear')

def almacenarTextoFichero(ruta_fichero):

    with open(ruta_fichero, 'r') as fichero:
        texto=fichero.read()
        return texto

def binarioATexto(fichero):

    return

###############funciones únicas para el código

def CrearMenuDesdeDiccionario():
    """Esta función utiliza las funciones CreacionDiccionarios y menuUniversal para crear
    un menú y dejar al usuario elegir una opción dentro del menú a partir de un fichero json.

    Returns:
        opcion: opción elegida por el usuario (válida).
    """

    ruta='./menus.json'
    limpiarTerminal()
    menuPrincipal=CreacionDiccionarios(ruta, 'menuPrincipal')
    opcion=menuUniversal(menuPrincipal)
    return opcion

def FicheroBinarioATexto():

    texto=almacenarTextoFichero('./binary_text.txt')
    return texto