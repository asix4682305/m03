dict={"clau": "valor",
      "clau1": "valor1"...}

dict[clau1]-> valor1

Son ordenats, pots accedir als elements amb un índex.
Estructura mutable, es poden afegir, eliminar items...

Si escribim dos valors diferents amb la misma clau, aquesta es quedarà amb el valor de l'últim.
dict={"a": "1"
      "a": "2"}
print(a)
2