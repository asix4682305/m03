#! /usr/bin/python3

import json
import os
import sys

def CreacionDiccionarios(ruta_archivo=str, elemento=str):
    """Funcion que convierte elementos de ficheros json en diccionarios python.

    Args:
        ruta_archivo (string): localización del archivo json.
        elemento (string): elemento que convertir en diccionario.

    Returns:
        diccionario: diccionario de python.
    """
    with open(ruta_archivo, "r") as archivo:
        datos_json = json.load(archivo)
    
    diccionario=datos_json[elemento]
    return diccionario

def menuUniversal(menu=dict):


    """Función que muestra datos con apariencia de menú y deja al usuario
    elegir una opción dentro de las existentes usando números.

    Args:
        menu (diccionario): diccionario de python con las opciones a mostrar.
        sleep (num): tiempo que pasa entre que aparece cada entrada del diccionario.

    Returns:
        opcion: opcion que ha elegido el usuario.
    """
    opciones=len(menu)

    while True:
        try:
            for x, y in menu.items():   #Display menu
                print(x, y)

            opcion=int(input('Introduce un número: '))
            if opcion <= opciones and opcion > 0:
                return opcion                
        except ValueError:
            print('Introduce un número de los indicados en pantalla.')
            os.system('clear')


def enterParaContinuar():
    """Espera a que el usuario le de a Enter para romper el bucle y continuar con el flujo.
    """
    while True:
        input("Pulsa Enter para continuar: ")
        break

def limpiarTerminal():

    """Función que limpia la terminal.

    Args:
        sleep (num): Tiempo que tarda en limpiarse la terminal.
    """
    os.system('clear')

###Funciones main

def displayFolderContent():
    """Esta función recibe por input una ruta, verifica si es un directorio,
    si lo es, muestra su contenido.
    """
    ruta_carpeta=str(input('Introduce la ruta del directorio: '))
    if os.path.isdir(ruta_carpeta):
        contenido=os.listdir(ruta_carpeta)
        for item in contenido:
            print(item)
    else:
        print("Error: el directorio no existe.")

def createEmptyFile():
    ruta_file=str(input('Ruta del archivo a crear (incluyendo nombre): '))

    if os.path.exists(ruta_file):
        print("El archivo ya existe.")
        si_o_no=str(input("¿Quieres reemplazarlo?(s/n) "))
        if si_o_no=='n':
            return 'Fichero no creado'
        elif si_o_no=='s':
            pass
        else:
            return "Error"
        with open(ruta_file, 'x') as file:
            pass

def main():
    menuPrincipal=CreacionDiccionarios('./menus.json', 'menuPrincipal')
    while True:
        opcion=menuUniversal(menuPrincipal)
        match opcion:
            case 1:
                #Display a folder's content
                displayFolderContent()
                enterParaContinuar()
                limpiarTerminal()
            case 2:
                #Create an empty file
                pass
            case 3:
                pass
            case 4:
                pass
            case 5:
                pass
            case 6:
                pass
            case 7:
                pass
            case 8:
                pass
            case 9:
                pass
            case 10:
                pass
            case 11:
                pass
            case 12:
                break